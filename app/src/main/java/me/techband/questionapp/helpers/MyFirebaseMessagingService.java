package me.techband.questionapp.helpers;

/**
 * Created by User on 5/26/2017.
 */

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import me.techband.questionapp.R;
import me.techband.questionapp.ui.activities.MainActivity;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
//        if (getSharedPreferences("AskMe", MODE_PRIVATE).getBoolean(Constants.EnableNotification, true)) {
            showNotification(remoteMessage.getNotification().getBody());
//        }
    }

    private void showNotification(String message) {

        Intent i = new Intent(this, MainActivity.class);

        Uri uri = Uri.parse("android.resource://me.techband.questionapp/" + R.raw.notification);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);


        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setAutoCancel(true)
                .setContentTitle(message)
                .setContentText(message)
                .setSmallIcon(R.drawable.logo2)
                .setSound(uri)
//                .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, builder.build());
    }
    }


