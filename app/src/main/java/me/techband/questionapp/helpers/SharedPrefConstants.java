package me.techband.questionapp.helpers;

/**
 * Created by moayed on 9/26/17.
 */

public class SharedPrefConstants {

    public static String loginObject = "loginObject";
    public static String cartObjects = "cartObjects";
    public static String badgeValue = "badgeValue";
    public static String searchIndexs = "searchIndexs";
    public static String wishList = "wishList";

    public static String Language = "Language";
    public static String Locale = "locale_override";
    public static String notificationKey = "notificationKey";
    public static String mainCatigory = "mainCatigory";
    public static String products = "products";
}
