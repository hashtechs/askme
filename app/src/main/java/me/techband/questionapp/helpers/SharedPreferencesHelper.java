package me.techband.questionapp.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

/**
 * Created by moayed on 9/26/17.
 */

public class SharedPreferencesHelper {


    public static void putSharedPreferencesObject(Context context, String key, Object shared_object) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = preferences.edit();
        edit.remove(key).commit();
        Gson gson = new Gson();
        String json = gson.toJson(shared_object); // myObject - instance of MyObject
        edit.putString(key, json);
        edit.commit();
    }

    public static Object getSharedPreferencesObject(Context context, String key, Class neededClass) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = preferences.getString(key, "");
        Object obj = gson.fromJson(json, neededClass);
        return obj;
    }


    public static void putSharedPreferencesString(Context context, String key, String val) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString(key, val);
        edit.commit();
    }

    public static String getSharedPreferencesString(Context context, String key, String _default) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, _default);
    }

//    public static void saveToWishList(Context context, String key, List<MainCatigoryModel.Item> list) {
//        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
//        SharedPreferences.Editor edit = preferences.edit();
//        Gson gson = new Gson();
//        String jsonFavorites = gson.toJson(list);
//        edit.putString(key, jsonFavorites);
//        edit.commit();
//    }
//
//    public static void saveBookmarkList(Context context, String key, List<MainCatigoryModel.Item> list) {
//        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
//        SharedPreferences.Editor edit = preferences.edit();
//        Gson gson = new Gson();
//        String jsonFavorites = gson.toJson(list);
//        edit.putString(key, jsonFavorites);
//        edit.commit();
//    }
//
//
//    public static ArrayList<MainCatigoryModel.Item> getBookmarkList(Context context, String key) {
//        List<MainCatigoryModel.Item> expertModels;
//        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
//
//        if (preferences.contains(key)) {
//            String json = preferences.getString(key, null);
//            Gson gson = new Gson();
//            MainCatigoryModel.Item[] expertModels1 = gson.fromJson(json,
//                    MainCatigoryModel.Item[].class);
//            expertModels = Arrays.asList(expertModels1);
//            expertModels = new ArrayList<MainCatigoryModel.Item>(expertModels);
//        } else
//            return null;
//
//        return (ArrayList<MainCatigoryModel.Item>) expertModels;
//    }
//
//
//    public static void addToCart(final Context context, MainCatigoryModel.Item product, String key) {
//        List<MainCatigoryModel.Item> expertModels = getBookmarkList(context, key);
//        if (expertModels == null)
//            expertModels = new ArrayList<MainCatigoryModel.Item>();
//        expertModels.add(product);
//        saveBookmarkList(context, key, expertModels);
//    }
//
//    public static void remove(Context context, String key) {
//        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
//        preferences.edit()
//                .remove(key)
//                .commit();
//    }
//
//    public static void removeCart(Context context, MainCatigoryModel.Item product, String key) {
//        int idItem = 0;
//        int idItems = 0;
//
//        ArrayList<MainCatigoryModel.Item> favorites = getBookmarkList(context, key);
//        if (favorites != null) {
//            for (int counter = 0; counter < favorites.size(); counter++) {
//                MainCatigoryModel.Item expertModel = favorites.get(counter);
//                try {
//                    idItem = Integer.parseInt(product.getItemId());
//                    idItems = Integer.parseInt(favorites.get(counter).getItemId());
//                } catch (NumberFormatException e) {
//                    e.printStackTrace();
//                }
//                if (idItem == idItems) {
//                    favorites.remove(counter);
//                    break;
//                }
//            }
//            saveBookmarkList(context, key, favorites);
//        }
//    }

    public static boolean clear(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.edit()
                .clear()
                .commit();

    }


}
