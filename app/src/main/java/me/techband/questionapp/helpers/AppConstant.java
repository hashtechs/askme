package me.techband.questionapp.helpers;

/**
 * Created by moayed on 9/21/17.
 */

public class AppConstant {

    public static final  int ANIMATION_DURATION = 100;
    public static final  int ANIMATION_DURATION_EXPAND = 100;
    public static final String POST = "POST";
    public static final String GET = "GET";
    public static final String GETQUESTIONS = "GETQUESTIONS";
    public static final String ADDQUESTION = "ADDQUESTION";
    public static final String ALLPOSTS = "ALLPOSTS";
    public static final String COMMENTS = "COMMENTS";



}
