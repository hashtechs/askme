package me.techband.questionapp.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;



public class EditTextCustomFont extends android.support.v7.widget.AppCompatEditText {
    public EditTextCustomFont(Context context, AttributeSet attrs) {
        super(context, attrs);
            this.setTypeface(Typeface.createFromAsset(context.getAssets(), "DroidArabicKufi.ttf"));
    }
}
