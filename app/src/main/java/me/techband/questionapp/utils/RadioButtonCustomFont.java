package me.techband.questionapp.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;


public class RadioButtonCustomFont extends android.support.v7.widget.AppCompatRadioButton {
    public RadioButtonCustomFont(Context context, AttributeSet attrs) {
        super(context, attrs);
            this.setTypeface(Typeface.createFromAsset(context.getAssets(), "DroidArabicKufi.ttf"));
    }
}
