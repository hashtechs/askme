package me.techband.questionapp.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import me.techband.questionapp.R;
import me.techband.questionapp.helpers.SharedPrefConstants;
import me.techband.questionapp.helpers.SharedPreferencesHelper;
import me.techband.questionapp.models.AddQuestionModel;
import me.techband.questionapp.models.UserModel;
import me.techband.questionapp.sweetdialog.SweetAlertDialog;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pl.droidsonroids.gif.GifImageView;

public class Utils {

    private static AVLoadingIndicatorView avLoadingIndicatorView;
    public static AlertDialog loading_view = null;
    private FragmentManager fragmentManager;


    public static void failureDialog(Context context, String error){
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context,
                SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setTitleText("عذرا");
        sweetAlertDialog.setContentText(error);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.cancel();
            }
        });
        sweetAlertDialog.show();
    }

    public static void successDialog(Context context, String msg){
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context,
                SweetAlertDialog.SUCCESS_TYPE);
        sweetAlertDialog.setTitleText("تمت العملية بنجاح");
        sweetAlertDialog.setContentText(msg);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.cancel();
            }
        });
        sweetAlertDialog.show();
    }


    public static boolean haveNetworkConnection(Activity activity) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }

        final boolean value = haveConnectedWifi || haveConnectedMobile;

        return value;
    }


    public static void dialogErrorInternet(final Activity activity) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View v = inflater.inflate(R.layout.check_internet_dialog, null);

        GifImageView gifImageView = v.findViewById(R.id.gif_internet);
        ButtonCustomFont btnRetry = v.findViewById(R.id.btnRetry);


        final Dialog dialog = new Dialog(activity, R.style.DialogThemeWhite);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (haveNetworkConnection(activity)) {
                    dialog.dismiss();
                }
            }
        });
        dialog.setContentView(v);
        dialog.show();
        Window window_register = dialog.getWindow();
        window_register.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
    }


    public static boolean isLogin(Context context)
    {
        UserModel userLoginModel = new UserModel();
        userLoginModel = (UserModel) SharedPreferencesHelper.getSharedPreferencesObject(context, SharedPrefConstants.loginObject,UserModel.class);
        if (userLoginModel==null || userLoginModel.equals(""))
        {
            return false;
        }else
        {
            return true;
        }
    }


    public static JSONObject parseValueToJson(Object value, boolean isSingleField){

        if (isSingleField){
            JSONObject fieldJson = new JSONObject();
            try {
                fieldJson.put("value", value);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            JSONArray jsonArray = new JSONArray();

            jsonArray.put(fieldJson);

            JSONObject undObj = new JSONObject();
            try {
                undObj.put("und", jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return  undObj;
        } else {
            JSONObject fieldJson = new JSONObject();
            try {
                fieldJson.put("value", value);
                fieldJson.put("format", null);
                fieldJson.put("safe_value", value);

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            JSONArray jsonArray = new JSONArray();

            jsonArray.put(fieldJson);

            JSONObject undObj = new JSONObject();
            try {
                undObj.put("und", jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return  undObj;
        }

    }


    public static JSONObject parseValueToJsonInt(Object value){

            JSONArray jsonArray = new JSONArray();
            jsonArray.put(value);
            JSONObject undObj = new JSONObject();
                try {
                    undObj.put("und", jsonArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            return  undObj;

            }


    public static Object setQuestionModel(String question){
        AddQuestionModel.Und und = new AddQuestionModel.Und();
        und.setValue(question);
        List<AddQuestionModel.Und>unds = new ArrayList<>();
        unds.add(und);

        return unds;
    }



    public static void showProccessDialog(Activity activity) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = activity.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.avi_progressbar, null);
        avLoadingIndicatorView = (AVLoadingIndicatorView) dialogView.findViewById(R.id.avi);
        dialogBuilder.setView(dialogView);
        loading_view = dialogBuilder.create();
        loading_view.getWindow().setBackgroundDrawableResource(R.color.transparent);
        loading_view.setCancelable(true);
        loading_view.show();
        avLoadingIndicatorView.show();
    }


    public static void dismissProccessDialog() {
        try {

            avLoadingIndicatorView.hide();
            loading_view.dismiss();
            loading_view.cancel();
            loading_view = null;


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isProgressShowing() {
        try {
            if (loading_view != null) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {

        }
        return true;

    }



    public static boolean isEmailValid(String email) {

        String regExpn = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches())
            return false;
        else
            return true;
    }



}
