package me.techband.questionapp.utils;

public class ColorsHelper {

    public static int getColor(int position) {
        switch (position % 4) {
            case 0:
                return 0xff1ba394;
            case 1:
                return 0xff744a6c;
            case 2:
                return 0xffe34c4e;
            default:
                return 0xffeca32a;
        }
    }
}
