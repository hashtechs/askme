package me.techband.questionapp.utils;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import me.techband.questionapp.R;

public class SpinnerUtil {

    private static SpinnerUtil mInstance;

    public static SpinnerUtil getInstance() {
        if (mInstance == null) {
            mInstance = new SpinnerUtil();
        }
        return mInstance;
    }

    private SpinnerUtil() {

    }

    public Spinner setupSpinner(Spinner spinner, int string_array, Context context){

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context,
                string_array, R.layout.spinner_layout);
        adapter.setDropDownViewResource(R.layout.spinner_row);
        spinner.setAdapter(adapter);

        return spinner;
    }


}
