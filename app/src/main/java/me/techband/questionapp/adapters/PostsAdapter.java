package me.techband.questionapp.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import me.techband.questionapp.R;
import me.techband.questionapp.models.QuestionModel;
import me.techband.questionapp.ui.activities.MainActivity;
import me.techband.questionapp.ui.fragments.QuestionDetailsFragment;
import me.techband.questionapp.utils.ColorsHelper;
import me.techband.questionapp.utils.TextviewCustomFont;
import me.techband.questionapp.utils.TextviewCustomFontBold;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int ITEM = 0;
    private static final int LOADING = 1;

    private List<QuestionModel> postsList;
    private Context context;

    private boolean isLoadingAdded = false;


    public PostsAdapter(Context context) {
        this.context = context;
        postsList = new ArrayList<>();

    }

    public List<QuestionModel> getPosts() {
        return postsList;
    }

    public void setPosts(List<QuestionModel> postsList) {
        this.postsList = postsList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View view = inflater.inflate(R.layout.row_progress, parent, false);
                viewHolder = new LoadingVH(view);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.row_post, parent, false);
        viewHolder = new PostsViewHolder(v1);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        QuestionModel result = postsList.get(position);

        switch (getItemViewType(position)) {
            case ITEM:
                final PostsViewHolder postsViewHolder = (PostsViewHolder) holder;

                postsViewHolder.tvName.setText(result.getQuestionerName().toString());
                postsViewHolder.tvQuestionContent.setText(result.getQuestion().toString());
                postsViewHolder.tvQuestionDate.setText(result.getQuestionDate().toString());
                int color = ColorsHelper.getColor(position);
                postsViewHolder.itemView.setBackgroundColor(color);

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((MainActivity) context).changeFragmentMethod(QuestionDetailsFragment.newInstance(postsList.get(position), ColorsHelper.getColor(position)), "");
                    }
                });

                break;

            case LOADING:
//                Do nothing
                break;
        }

    }

    @Override
    public int getItemCount() {
        return postsList == null ? 0 : postsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == postsList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }


    public void add(QuestionModel r) {
        postsList.add(r);
        notifyItemInserted(postsList.size() - 1);
    }

    public void addAll(List<QuestionModel> moveResults) {
        for (QuestionModel result : moveResults) {
            add(result);
        }
    }

    public void remove(QuestionModel r) {
        int position = postsList.indexOf(r);
        if (position > -1) {
            postsList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new QuestionModel());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = postsList.size() - 1;
        QuestionModel result = getItem(position);

        if (result != null) {
            postsList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public QuestionModel getItem(int position) {
        return postsList.get(position);
    }


    protected class PostsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvColor)
        TextView tvColor;

        @BindView(R.id.tvName)
        TextviewCustomFontBold tvName;

        @BindView(R.id.tvQuestionDate)
        TextviewCustomFont tvQuestionDate;

        @BindView(R.id.tvQuestionContent)
        TextviewCustomFontBold tvQuestionContent;


        public PostsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    protected class LoadingVH extends RecyclerView.ViewHolder {

        public LoadingVH(View itemView) {
            super(itemView);
        }
    }


}
