package me.techband.questionapp.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import me.techband.questionapp.helpers.SharedPrefConstants;
import me.techband.questionapp.helpers.SharedPreferencesHelper;
import me.techband.questionapp.models.QuestionModel;
import me.techband.questionapp.models.UserModel;
import me.techband.questionapp.ui.activities.MainActivity;
import me.techband.questionapp.ui.fragments.QuestionDetailsFragment;
import me.techband.questionapp.utils.ColorsHelper;
import me.techband.questionapp.R;
import me.techband.questionapp.utils.TextviewCustomFont;
import me.techband.questionapp.utils.TextviewCustomFontBold;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.ColorViewHolder> {

    private Activity context;
    private List<QuestionModel> questionsList;
    private boolean isAnswerd;

    public QuestionAdapter(Activity context, List<QuestionModel> questionsList, boolean isAnswerd) {
        this.context = context;
        this.questionsList = questionsList;
        this.isAnswerd = isAnswerd;

    }

    @Override
    public ColorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_post, parent, false);
        ColorViewHolder colorViewHolder = new ColorViewHolder(view);


        return colorViewHolder;
    }

    @Override
    public void onBindViewHolder(ColorViewHolder holder, int position) {

        UserModel userLoginModel = (UserModel) SharedPreferencesHelper.getSharedPreferencesObject(context, SharedPrefConstants.loginObject, UserModel.class);


        int color = ColorsHelper.getColor(position);
        holder.itemView.setBackgroundColor(color);
        holder.tvName.setText(userLoginModel.getFullname());
        holder.tvQuestionContent.setText(questionsList.get(position).getQuestion().toString());
        holder.tvQuestionDate.setText(questionsList.get(position).getQuestionDate().toString());


        holder.itemView.setOnClickListener(v -> {
            int adapterPosition = holder.getAdapterPosition();
            if (adapterPosition != RecyclerView.NO_POSITION) {
                ((MainActivity) context).changeFragmentMethod(QuestionDetailsFragment.newInstance(questionsList.get(adapterPosition), ColorsHelper.getColor(position)), "see All");
            }
        });


    }

    @Override
    public int getItemCount() {
        return (null != questionsList ? questionsList.size() : 0);
    }

    public static class ColorViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvColor)
        TextView tvColor;

        @BindView(R.id.tvName)
        TextviewCustomFontBold tvName;

        @BindView(R.id.tvQuestionDate)
        TextviewCustomFont tvQuestionDate;

        @BindView(R.id.tvQuestionContent)
        TextviewCustomFontBold tvQuestionContent;

        public ColorViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
