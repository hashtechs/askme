package me.techband.questionapp.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import me.techband.questionapp.R;
import me.techband.questionapp.models.CommentModel;
import me.techband.questionapp.models.QuestionModel;
import me.techband.questionapp.ui.activities.MainActivity;
import me.techband.questionapp.ui.fragments.QuestionDetailsFragment;
import me.techband.questionapp.utils.ColorsHelper;
import me.techband.questionapp.utils.TextviewCustomFont;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ColorViewHolder> {

    private Activity context;
    private CommentModel[] commentList;

    public CommentAdapter(Activity context, CommentModel[] commentList) {
        this.context = context;
        this.commentList = commentList;

    }

    @Override
    public ColorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_comment, parent, false);
        ColorViewHolder colorViewHolder = new ColorViewHolder(view);


        return colorViewHolder;
    }

    @Override
    public void onBindViewHolder(ColorViewHolder holder, final int position) {

        int color = ColorsHelper.getColor(position);
        holder.itemView.setBackgroundColor(color);
//        holder.tvColor.setText("#" + Integer.toHexString(color));
        holder.tvName.setText(commentList[position].getSubject().toString());
        holder.tvComment.setText(commentList[position].getCommentBody().getUnd().get(0).getValue().toString());


    }

    @Override
    public int getItemCount() {
        return (null != commentList ? commentList.length : 0);
    }

    public static class ColorViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvColor)
        TextView tvColor;

        @BindView(R.id.tvName)
        TextviewCustomFont tvName;

        @BindView(R.id.tvComment)
        TextviewCustomFont tvComment;


        public ColorViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
