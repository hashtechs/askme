package me.techband.questionapp.managers;

import android.content.Context;

import me.techband.questionapp.helpers.AppConstant;
import me.techband.questionapp.interfaces.ApiCallResponse;
import me.techband.questionapp.interfaces.ApiCallResponseCustom;
import me.techband.questionapp.models.CommentModel;
import me.techband.questionapp.models.QuestionModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

import static me.techband.questionapp.managers.ConnectionManager.APIService.BASE_URL;


/**
 * Created by moayed on 12/10/17.
 */

public class ConnectionManager {


    public static OkHttpClient getHeader(final String token, final String cookie) {


        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        OkHttpClient okClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addNetworkInterceptor(
                        new Interceptor() {
                            @Override
                            public okhttp3.Response intercept(Chain chain) throws IOException {
                                Request request = null;
                                Request original = chain.request();
                                Request.Builder requestBuilder = original.newBuilder()
                                        // .addHeader("Authorization","Basic YWRtaW46YWRtaW4=")
                                        .addHeader("Content-Type", "application/json")
                                        .addHeader("Cookie", cookie)
                                        .addHeader("X-CSRF-Token", token);
                                request = requestBuilder.build();

                                return chain.proceed(request);
                            }
                        })
                .build();
        return okClient;

    }

    public static void doRequest(Context context, final String type, String apiRoute, Map<String, Object> Params, String token, String cookie, ApiCallResponse apiCallResponse) {
        switch (type) {
            case AppConstant.GET:
                GET(context, apiRoute, Params, apiCallResponse);
                break;
            case AppConstant.POST:
                POST(context, apiRoute, Params, apiCallResponse);
                break;
            case AppConstant.ADDQUESTION:
                POST(context, apiRoute, Params, token, cookie, apiCallResponse);
                break;
        }
    }


    public static void doRequestCustom(final String type, Map<String, Object> Params, ApiCallResponseCustom apiCallResponse) {
        switch (type) {
            case AppConstant.GETQUESTIONS:
                GetQuetions(Params, apiCallResponse);
                break;
            case AppConstant.ALLPOSTS:
                getAllPosts(Params, apiCallResponse);
                break;
        }
    }


    public static void doRequestCustom(final String type, String id, Map<String,Object> params, ApiCallResponseCustom apiCallResponse) {
        switch (type) {
            case AppConstant.COMMENTS:
                getCommentsById(apiCallResponse, id, params);
                break;
        }
    }

    public static void GET(Context context, String URl, Map<String, Object> Params, final ApiCallResponse callResponse) {
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(100, TimeUnit.SECONDS)
                .readTimeout(100, TimeUnit.SECONDS).build();
        Retrofit retrofit = new Retrofit.Builder()

                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<ResponseBody> call = service.GET(URl, Params);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() == null) {
                        callResponse.onSuccess(response.body().toString(), response.message());
                    } else {
                        callResponse.onSuccess(response.body().string(), response.message());
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callResponse.onFailure(t.toString());
            }
        });

    }

    public static void GetQuetions(Map<String, Object> Params, final ApiCallResponseCustom callResponse) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(getHeader(Params.get("token").toString(), Params.get("cookie").toString()))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<QuestionModel[]> call = service.getQuestion(Params);
        call.enqueue(new Callback<QuestionModel[]>() {
            @Override
            public void onResponse(Call<QuestionModel[]> call, Response<QuestionModel[]> response) {
                try {
                    callResponse.onSuccess(response.body(), response.message());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<QuestionModel[]> call, Throwable t) {
                callResponse.onFailure(t.toString());
            }
        });
    }

    public static void getAllPosts(Map<String, Object> Params, final ApiCallResponseCustom callResponse) {
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(100, TimeUnit.SECONDS)
                .readTimeout(100, TimeUnit.SECONDS).build();
        Retrofit retrofit = new Retrofit.Builder()

                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<QuestionModel[]> call = service.getAllPosts(Params);
        call.enqueue(new Callback<QuestionModel[]>() {
            @Override
            public void onResponse(Call<QuestionModel[]> call, Response<QuestionModel[]> response) {
                try {
                    callResponse.onSuccess(response.body(), response.message());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<QuestionModel[]> call, Throwable t) {
                callResponse.onFailure(t.toString());
            }
        });

    }

    public static void getCommentsById(final ApiCallResponseCustom callResponse, String id, Map<String,Object> params) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(getHeader(params.get("token").toString(), params.get("cookie").toString()))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<CommentModel[]> call = service.getCommentsById(id,params);
        call.enqueue(new Callback<CommentModel[]>() {
            @Override
            public void onResponse(Call<CommentModel[]> call, Response<CommentModel[]> response) {
                try {
                    callResponse.onSuccess(response.body(), response.message());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<CommentModel[]> call, Throwable t) {
                callResponse.onFailure(t.toString());
            }
        });

    }

    public static void POST(Context context, String URl, Map<String, Object> Params, final ApiCallResponse callResponse) {


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<ResponseBody> call = service.addQuestion(URl, Params);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int code = response.code();
                String msg = response.message();
                if (code == 200) {
                    try {
                        callResponse.onSuccess(response.body().string(), response.message());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (code == 401 || code == 406) {
                    callResponse.onSuccess(response.message().toString(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callResponse.onFailure(t.toString());
            }
        });
    }


    public static void POST(Context context, String URl, Object Params, String token, String cookie, final ApiCallResponse callResponse) {


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(getHeader(token, cookie))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        APIService service = retrofit.create(APIService.class);
        Call<ResponseBody> call = service.POSTObject(URl, Params);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int code = response.code();
                String msg = response.message();
                if (code == 200) {
                    try {
                        callResponse.onSuccess(response.body().string(), response.message());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (code == 401 || code == 406) {
                    callResponse.onSuccess(response.message(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callResponse.onFailure(t.toString());
            }
        });
    }


    public interface APIService {

        String BASE_URL = "http://techband.me/question_app/api/v1/";

        @GET()
        Call<ResponseBody> GET(@Url String url, @QueryMap Map<String, Object> params);

        @POST
        Call<ResponseBody> addQuestion(@Url String url, @Body Map<String, Object> params);

        @Headers("cache-control:no-cache")
        @GET("my_questions")
        Call<QuestionModel[]> getQuestion(@QueryMap Map<String, Object> params);

        @GET("comments_allowed_public_questions")
        Call<QuestionModel[]> getAllPosts(@QueryMap Map<String, Object> params);

        @GET("node/{id}/comments")
        Call<CommentModel[]> getCommentsById(@Path("id") String id, @QueryMap Map<String,Object> params);

        @POST
        Call<ResponseBody> POSTObject(@Url String url, @Body Object object);

        @PUT("user/{id}")
        Call<ResponseBody> PUTObject(@Path("id") String id, @Body Object object);
    }
}
