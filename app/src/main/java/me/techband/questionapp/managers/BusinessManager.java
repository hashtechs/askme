package me.techband.questionapp.managers;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import me.techband.questionapp.helpers.AppConstant;
import me.techband.questionapp.helpers.SharedPrefConstants;
import me.techband.questionapp.helpers.SharedPreferencesHelper;
import me.techband.questionapp.interfaces.ApiCallResponse;
import me.techband.questionapp.interfaces.ApiCallResponseCustom;
import me.techband.questionapp.models.AddCommentModel;
import me.techband.questionapp.models.AddQuestionModel;
import me.techband.questionapp.models.CommentModel;
import me.techband.questionapp.models.ContactModel;
import me.techband.questionapp.models.QuestionModel;
import me.techband.questionapp.models.RefreshTokenModel;
import me.techband.questionapp.models.RegisterModel;
import me.techband.questionapp.models.UpdateModel;
import me.techband.questionapp.models.UserModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static me.techband.questionapp.interfaces.ApiConstants.Login;
import static me.techband.questionapp.managers.ConnectionManager.APIService.BASE_URL;


/**
 * Created by moayed on 12/16/17.
 */

public class BusinessManager {


    public void loginApiCall(final Context context, String username, String password, final ApiCallResponse callResponse) {

        Map<String, Object> Params = new HashMap<>();
        Params.put("username", username);
        Params.put("password", password);
        ConnectionManager.doRequest(context, AppConstant.POST, Login, Params, "", "", new ApiCallResponse() {
            @Override
            public void onSuccess(Object responseObject, String responseMessage) {

                try {

                    Gson gson = new Gson();
                    String json = responseObject.toString();
                    if (json.equals("Unauthorized : Wrong username or password.")) {
                        callResponse.onSuccess(responseObject, responseMessage);
                    } else {
                        UserModel parseObject = gson.fromJson(json, UserModel.class);
                        callResponse.onSuccess(parseObject, responseMessage);
                    }

//                    }
                } catch (JsonSyntaxException e) {
//                    Toast.makeText(context, "Username or Password Incorrect ", Toast.LENGTH_LONG).show();
//                    Utils.failureDialog(context, "اسم الدخول او كلمة المرور غير صحيحة");

                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String errorResponse) {
                callResponse.onFailure(errorResponse);
                Log.wtf("loginApiCall", errorResponse + "");
            }
        });
    }


    public void registerApiCall(final Context context, String username, String email, String password, String gender, String nationality, String fullname, final ApiCallResponse callResponse) {

        RegisterModel.Und undGender = new RegisterModel.Und();
        undGender.setValue(gender);
        undGender.setSafeValue(gender);
        undGender.setFormat(null);
        List<RegisterModel.Und> undsGender = new ArrayList<>();
        undsGender.add(undGender);
        RegisterModel.FieldGender fieldGender = new RegisterModel.FieldGender();
        fieldGender.setUnd(undsGender);


        RegisterModel.Und_ undNationality = new RegisterModel.Und_();
        undNationality.setValue(nationality);
        undNationality.setSafeValue(nationality);
        undNationality.setFormat(null);
        List<RegisterModel.Und_> undsNationality = new ArrayList<>();
        undsNationality.add(undNationality);
        RegisterModel.FieldNationality fieldNationality = new RegisterModel.FieldNationality();
        fieldNationality.setUnd(undsNationality);


        RegisterModel.Und__ undFullname = new RegisterModel.Und__();
        undFullname.setValue(fullname);
        undFullname.setSafeValue(nationality);
        undFullname.setFormat(null);
        List<RegisterModel.Und__> undsFullname = new ArrayList<>();
        undsFullname.add(undFullname);
        RegisterModel.FieldFullName fieldFullname = new RegisterModel.FieldFullName();
        fieldFullname.setUnd(undsFullname);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ConnectionManager.APIService service = retrofit.create(ConnectionManager.APIService.class);
        Call<ResponseBody> call = service.POSTObject("user/register", new RegisterModel(username, email, password, fieldGender, fieldNationality, fieldFullname));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int code = response.code();
                String msg = response.message();
                if (code == 200) {
                    try {
                        callResponse.onSuccess(response.body().string(), response.message());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (code == 401 || code == 406) {
                    callResponse.onSuccess(response.body(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callResponse.onFailure(t.toString());
            }
        });
    }


    public void updateProfileCall(final Context context, String id, String username, String email, String gender, String nationality, String fullname, final ApiCallResponse callResponse) {

        UpdateModel.Und undGender = new UpdateModel.Und();
        undGender.setValue(gender);
        List<UpdateModel.Und> undsGender = new ArrayList<>();
        undsGender.add(undGender);
        UpdateModel.FieldGender fieldGender = new UpdateModel.FieldGender();
        fieldGender.setUnd(undsGender);


        UpdateModel.Und_ undNationality = new UpdateModel.Und_();
        undGender.setValue(nationality);
        List<UpdateModel.Und_> undsNationality = new ArrayList<>();
        undsNationality.add(undNationality);
        UpdateModel.FieldNationality fieldNationality = new UpdateModel.FieldNationality();
        fieldNationality.setUnd(undsNationality);


        UpdateModel.Und__ undFullname = new UpdateModel.Und__();
        undFullname.setValue(fullname);
        List<UpdateModel.Und__> undsFullname = new ArrayList<>();
        undsFullname.add(undFullname);
        UpdateModel.FieldFullName fieldFullname = new UpdateModel.FieldFullName();
        fieldFullname.setUnd(undsFullname);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ConnectionManager.APIService service = retrofit.create(ConnectionManager.APIService.class);
        Call<ResponseBody> call = service.PUTObject(id, new UpdateModel(username, email, fieldGender, fieldNationality, fieldFullname));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int code = response.code();
                String msg = response.message();
                if (code == 200) {
                    try {
                        callResponse.onSuccess(response.body().string(), response.message());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (code == 401 || code == 406) {
                    callResponse.onSuccess(response.message(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callResponse.onFailure(t.toString());
            }
        });
    }


    public void refreshToken(final Context context, String id, String currentPass, String token, String platform, final ApiCallResponse callResponse) {

        RefreshTokenModel.Und undToken = new RefreshTokenModel.Und();
        undToken.setValue(token);
        undToken.setFormat("null");
        undToken.setSafeValue(token);
        List<RefreshTokenModel.Und> undsToken = new ArrayList<>();
        undsToken.add(undToken);
        RefreshTokenModel.FieldFirebaseToken fieldToken = new RefreshTokenModel.FieldFirebaseToken();
        fieldToken.setUnd(undsToken);


        RefreshTokenModel.Und_ undPlatform = new RefreshTokenModel.Und_();
        undPlatform.setValue(platform);
        undPlatform.setFormat("null");
        undPlatform.setSafeValue(platform);
        List<RefreshTokenModel.Und_> undsPlatform = new ArrayList<>();
        undsPlatform.add(undPlatform);
        RefreshTokenModel.FieldOsPlatform fieldPlatform = new RefreshTokenModel.FieldOsPlatform();
        fieldPlatform.setUnd(undsPlatform);



        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ConnectionManager.APIService service = retrofit.create(ConnectionManager.APIService.class);
        Call<ResponseBody> call = service.PUTObject(id, new RefreshTokenModel(id, currentPass, fieldToken, fieldPlatform));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int code = response.code();
                String msg = response.message();
                if (code == 200) {
                    try {
                        callResponse.onSuccess(response.body().string(), response.message());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (code == 401 || code == 406) {
                    callResponse.onSuccess(response.message(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callResponse.onFailure(t.toString());
            }
        });
    }


    public void changePassword(final Context context, String currentPass, String pass, String id ,final ApiCallResponse callResponse) {


        UserModel userLoginModel = (UserModel) SharedPreferencesHelper.getSharedPreferencesObject(context, SharedPrefConstants.loginObject, UserModel.class);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(getHeader(userLoginModel.getToken(),userLoginModel.getSession_cookie()))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ConnectionManager.APIService service = retrofit.create(ConnectionManager.APIService.class);
        Call<ResponseBody> call = service.PUTObject(userLoginModel.getUid(), new UpdateModel(currentPass, pass, id));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int code = response.code();
                String msg = response.message();
                if (code == 200) {
                    try {
                        callResponse.onSuccess(response.body().string(), response.message());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (code == 401 || code == 406) {
                    callResponse.onSuccess(response.message(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callResponse.onFailure(t.toString());
            }
        });
    }


    public void getMyQuestions(Context context, int uid, final ApiCallResponseCustom callResponse) {
        UserModel userLoginModel = (UserModel) SharedPreferencesHelper.getSharedPreferencesObject(context, SharedPrefConstants.loginObject, UserModel.class);

        Map<String, Object> Params = new HashMap<>();
        Params.put("uid", uid);
        Params.put("token", userLoginModel.getToken());
        Params.put("cookie", userLoginModel.getSession_cookie());
        ConnectionManager.doRequestCustom(AppConstant.GETQUESTIONS, Params, new ApiCallResponseCustom() {
            @Override
            public void onSuccess(Object[] responseObject, String responseMessage) {

                QuestionModel[] objects = (QuestionModel[]) responseObject;


                callResponse.onSuccess(objects, responseMessage);
            }

            @Override
            public void onFailure(String errorResponse) {
                callResponse.onFailure(errorResponse);
                Log.wtf("GetALLCategoryWithItem", errorResponse + "");
            }
        });

    }


    public void getAllPosts(int page, final ApiCallResponseCustom callResponse) {

        Map<String, Object> Params = new HashMap<>();
        Params.put("page", page);
        ConnectionManager.doRequestCustom(AppConstant.ALLPOSTS, Params, new ApiCallResponseCustom() {
            @Override
            public void onSuccess(Object[] responseObject, String responseMessage) {
                QuestionModel[] objects = (QuestionModel[]) responseObject;
                callResponse.onSuccess(objects, responseMessage);
            }

            @Override
            public void onFailure(String errorResponse) {
                callResponse.onFailure(errorResponse);
            }
        });

    }


    public void getCommentsById(Context context, final ApiCallResponseCustom callResponse, String id) {
        UserModel userLoginModel = (UserModel) SharedPreferencesHelper.getSharedPreferencesObject(context, SharedPrefConstants.loginObject, UserModel.class);


        Map<String, Object> Params = new HashMap<>();
        Params.put("token", userLoginModel.getToken());
        Params.put("cookie", userLoginModel.getSession_cookie());

        ConnectionManager.doRequestCustom(AppConstant.COMMENTS, id, Params, new ApiCallResponseCustom() {
            @Override
            public void onSuccess(Object[] responseObject, String responseMessage) {
                CommentModel[] objects = (CommentModel[]) responseObject;
                callResponse.onSuccess(objects, responseMessage);
            }

            @Override
            public void onFailure(String errorResponse) {
                callResponse.onFailure(errorResponse);
            }
        });

    }


    public void addQuestions(final Context context, final ApiCallResponse callResponse, Object... questionData) {

        AddQuestionModel.Und undQuestion = new AddQuestionModel.Und();
        undQuestion.setValue(questionData[0].toString());
        List<AddQuestionModel.Und> undsQuestion = new ArrayList<>();
        undsQuestion.add(undQuestion);
        AddQuestionModel.FieldQuestion fieldQuestion = new AddQuestionModel.FieldQuestion();
        fieldQuestion.setUnd(undsQuestion);

        AddQuestionModel.Und_ undQuestionerName = new AddQuestionModel.Und_();
        undQuestionerName.setValue(questionData[1].toString());
        List<AddQuestionModel.Und_> undsQuestionerName = new ArrayList<>();
        undsQuestionerName.add(undQuestionerName);
        AddQuestionModel.FieldQuestionerName fieldQuestionerName = new AddQuestionModel.FieldQuestionerName();
        fieldQuestionerName.setUnd(undsQuestionerName);


        AddQuestionModel.Und__ undQuestionerCountry = new AddQuestionModel.Und__();
        undQuestionerCountry.setValue(questionData[2].toString());
        List<AddQuestionModel.Und__> undsQuestionerCountry = new ArrayList<>();
        undsQuestionerCountry.add(undQuestionerCountry);
        AddQuestionModel.FieldQuestionerCountry fieldQuestionerCountry = new AddQuestionModel.FieldQuestionerCountry();
        fieldQuestionerCountry.setUnd(undsQuestionerCountry);


        AddQuestionModel.Und___ undQuestionerCity = new AddQuestionModel.Und___();
        undQuestionerCity.setValue(questionData[3].toString());
        List<AddQuestionModel.Und___> undsQuestionerCity = new ArrayList<>();
        undsQuestionerCity.add(undQuestionerCity);
        AddQuestionModel.FieldQuestionerCity fieldQuestionerCity = new AddQuestionModel.FieldQuestionerCity();
        fieldQuestionerCity.setUnd(undsQuestionerCity);


        AddQuestionModel.Und____ undQuestionerAge = new AddQuestionModel.Und____();
        undQuestionerAge.setValue(questionData[4].toString());
        List<AddQuestionModel.Und____> undsQuestionerAge = new ArrayList<>();
        undsQuestionerAge.add(undQuestionerAge);
        AddQuestionModel.FieldQuestionerAge fieldQuestionerAge = new AddQuestionModel.FieldQuestionerAge();
        fieldQuestionerAge.setUnd(undsQuestionerAge);


        AddQuestionModel.Und_____ undQuestionerSex = new AddQuestionModel.Und_____();
        undQuestionerSex.setValue(questionData[5].toString());
        List<AddQuestionModel.Und_____> undsQuestionerSex = new ArrayList<>();
        undsQuestionerSex.add(undQuestionerSex);
        AddQuestionModel.FieldQuestionerSex fieldQuestionerSex = new AddQuestionModel.FieldQuestionerSex();
        fieldQuestionerSex.setUnd(undsQuestionerSex);


        AddQuestionModel.Und______ undQuestionAnswer = new AddQuestionModel.Und______();
        undQuestionAnswer.setValue(questionData[6].toString());
        List<AddQuestionModel.Und______> undsQuestionAnswer = new ArrayList<>();
        undsQuestionAnswer.add(undQuestionAnswer);
        AddQuestionModel.FieldQuestionAnswer fieldQuestionAnswer = new AddQuestionModel.FieldQuestionAnswer();
        fieldQuestionAnswer.setUnd(undsQuestionAnswer);


        AddQuestionModel.Und_______ undQuestionType = new AddQuestionModel.Und_______();
        undQuestionType.setValue(questionData[7].toString());
        List<AddQuestionModel.Und_______> undsQuestionType = new ArrayList<>();
        undsQuestionType.add(undQuestionType);
        AddQuestionModel.FieldQuestionType fieldQuestionType = new AddQuestionModel.FieldQuestionType();
        fieldQuestionType.setUnd(undsQuestionType);


        List<Integer> undsfieldUid = new ArrayList<>();
        undsfieldUid.add((Integer) questionData[8]);

        AddQuestionModel.FieldUid fieldfieldUid = new AddQuestionModel.FieldUid();
        fieldfieldUid.setUnd(undsfieldUid);


        List<Integer> undsCommentsAllowed = new ArrayList<>();
        undsCommentsAllowed.add((Integer) questionData[9]);
        AddQuestionModel.FieldCommentsAllowed fieldCommentsAllowed = new AddQuestionModel.FieldCommentsAllowed();
        fieldCommentsAllowed.setUnd(undsCommentsAllowed);


        List<Integer> undsIsPublic = new ArrayList<>();
        undsIsPublic.add((Integer) questionData[10]);
        AddQuestionModel.FieldIsPublic fieldIsPublic = new AddQuestionModel.FieldIsPublic();
        fieldIsPublic.setUnd(undsIsPublic);


        UserModel userLoginModel = (UserModel) SharedPreferencesHelper.getSharedPreferencesObject(context, SharedPrefConstants.loginObject, UserModel.class);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(getHeader(userLoginModel.getToken(), userLoginModel.getSession_cookie()))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ConnectionManager.APIService service = retrofit.create(ConnectionManager.APIService.class);
        Call<ResponseBody> call = service.POSTObject("node", new AddQuestionModel("question", "question", fieldQuestion, fieldQuestionerName, fieldQuestionerCountry, fieldQuestionerCity, fieldQuestionerAge, fieldQuestionerSex, fieldQuestionAnswer, fieldQuestionType, fieldfieldUid, fieldCommentsAllowed, fieldIsPublic));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int code = response.code();
                String msg = response.message();
                if (code == 200) {
                    try {
                        callResponse.onSuccess(response.body().string(), response.message());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (code == 401 || code == 406) {
                    callResponse.onSuccess(response.message(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callResponse.onFailure(t.toString());
            }
        });
//
//        UserModel userLoginModel = (UserModel) SharedPreferencesHelper.getSharedPreferencesObject(context, SharedPrefConstants.loginObject,UserModel.class);
//
//
//        ConnectionManager.doRequest(context, AppConstant.ADDQUESTION, ADDQUESTION, Params, userLoginModel.getToken(), userLoginModel.getSession_cookie(), new ApiCallResponse() {
//            @Override
//            public void onSuccess(Object responseObject, String responseMessage) {
//
//                try {
//                    Gson gson = new Gson();
//                    String json = responseObject.toString();
//                    UserModel parseObject = gson.fromJson(json, UserModel.class);
//                    callResponse.onSuccess(parseObject, responseMessage);
//                } catch (JsonSyntaxException e) {
//                    e.printStackTrace();
//                }
//
//
//                if (responseMessage.contains("Not Acceptable")){
//                    Utils.failureDialog(context, "Invalid Json Format");
//                }
//
//
//            }
//
//            @Override
//            public void onFailure(String errorResponse) {
//                callResponse.onFailure(errorResponse);
//                Log.wtf("addQuestion", errorResponse + "");
//            }
//        });

    }


    public void addContact(final Context context, final ApiCallResponse callResponse, Object... questionData) {

        ContactModel.Und undUsername = new ContactModel.Und();
        undUsername.setValue(questionData[0].toString());
        List<ContactModel.Und> undsUsername = new ArrayList<>();
        undsUsername.add(undUsername);
        ContactModel.FieldUserName fieldUsername = new ContactModel.FieldUserName();
        fieldUsername.setUnd(undsUsername);

        ContactModel.Und_ undEmail = new ContactModel.Und_();
        undEmail.setValue(questionData[1].toString());
        List<ContactModel.Und_> undsEmail = new ArrayList<>();
        undsEmail.add(undEmail);
        ContactModel.FieldEmailAddress fieldEmail = new ContactModel.FieldEmailAddress();
        fieldEmail.setUnd(undsEmail);


        ContactModel.Und__ undMobile = new ContactModel.Und__();
        undMobile.setValue(questionData[2].toString());
        List<ContactModel.Und__> undsMobile = new ArrayList<>();
        undsMobile.add(undMobile);
        ContactModel.FieldContactUsPhoneNum fieldMobile = new ContactModel.FieldContactUsPhoneNum();
        fieldMobile.setUnd(undsMobile);


        ContactModel.Und___ undDeviceType = new ContactModel.Und___();
        undDeviceType.setValue(questionData[3].toString());
        List<ContactModel.Und___> undsDeviceType = new ArrayList<>();
        undsDeviceType.add(undDeviceType);
        ContactModel.FieldDeviceType fieldDeviceType = new ContactModel.FieldDeviceType();
        fieldDeviceType.setUnd(undsDeviceType);


        ContactModel.Und____ undBody = new ContactModel.Und____();
        undBody.setValue(questionData[4].toString());
        List<ContactModel.Und____> undsBody = new ArrayList<>();
        undsBody.add(undBody);
        ContactModel.FieldBody fieldBody = new ContactModel.FieldBody();
        fieldBody.setUnd(undsBody);


        UserModel userLoginModel = (UserModel) SharedPreferencesHelper.getSharedPreferencesObject(context, SharedPrefConstants.loginObject, UserModel.class);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(getHeader(userLoginModel.getToken(), userLoginModel.getSession_cookie()))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ConnectionManager.APIService service = retrofit.create(ConnectionManager.APIService.class);
        Call<ResponseBody> call = service.POSTObject("node", new ContactModel(questionData[5].toString(), "contact_us", fieldUsername, fieldEmail, fieldMobile, fieldDeviceType, fieldBody));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int code = response.code();
                String msg = response.message();
                if (code == 200) {
                    try {
                        callResponse.onSuccess(response.body().string(), response.message());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (code == 401 || code == 406) {
                    callResponse.onSuccess(response.message(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callResponse.onFailure(t.toString());
            }
        });
    }


    public void addComment(final Context context, final ApiCallResponse callResponse, Object... questionData) {


        AddCommentModel.Und undCommentBody = new AddCommentModel.Und();
        undCommentBody.setValue(questionData[3].toString());
        List<AddCommentModel.Und> undsCommentBody = new ArrayList<>();
        undsCommentBody.add(undCommentBody);
        AddCommentModel.CommentBody fieldCommentBody = new AddCommentModel.CommentBody();
        fieldCommentBody.setUnd(undsCommentBody);


        UserModel userLoginModel = (UserModel) SharedPreferencesHelper.getSharedPreferencesObject(context, SharedPrefConstants.loginObject, UserModel.class);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(getHeader(userLoginModel.getToken(), userLoginModel.getSession_cookie()))
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ConnectionManager.APIService service = retrofit.create(ConnectionManager.APIService.class);
        Call<ResponseBody> call = service.POSTObject("comment", new AddCommentModel(questionData[0].toString(), questionData[1].toString(), questionData[2].toString(), fieldCommentBody));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int code = response.code();
                String msg = response.message();
                Log.e("dfgdfgdfg", msg);
                if (code == 200) {
                    try {
                        callResponse.onSuccess(response.body().string(), response.message());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (code == 401 || code == 406) {
                    callResponse.onSuccess(response.message(), response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                callResponse.onFailure(t.toString());
            }
        });
//
//        UserModel userLoginModel = (UserModel) SharedPreferencesHelper.getSharedPreferencesObject(context, SharedPrefConstants.loginObject,UserModel.class);
//
//
//        ConnectionManager.doRequest(context, AppConstant.ADDQUESTION, ADDQUESTION, Params, userLoginModel.getToken(), userLoginModel.getSession_cookie(), new ApiCallResponse() {
//            @Override
//            public void onSuccess(Object responseObject, String responseMessage) {
//
//                try {
//                    Gson gson = new Gson();
//                    String json = responseObject.toString();
//                    UserModel parseObject = gson.fromJson(json, UserModel.class);
//                    callResponse.onSuccess(parseObject, responseMessage);
//                } catch (JsonSyntaxException e) {
//                    e.printStackTrace();
//                }
//
//
//                if (responseMessage.contains("Not Acceptable")){
//                    Utils.failureDialog(context, "Invalid Json Format");
//                }
//
//
//            }
//
//            @Override
//            public void onFailure(String errorResponse) {
//                callResponse.onFailure(errorResponse);
//                Log.wtf("addQuestion", errorResponse + "");
//            }
//        });

    }


    public static OkHttpClient getHeader(final String token, final String cookie) {


        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        OkHttpClient okClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addNetworkInterceptor(
                        new Interceptor() {
                            @Override
                            public okhttp3.Response intercept(Chain chain) throws IOException {
                                Request request = null;
                                Request original = chain.request();
                                Request.Builder requestBuilder = original.newBuilder()
                                        // .addHeader("Authorization","Basic YWRtaW46YWRtaW4=")
                                        .addHeader("Content-Type", "application/json")
                                        .addHeader("Cookie", cookie)
                                        .addHeader("X-CSRF-Token", token);
                                request = requestBuilder.build();

                                return chain.proceed(request);
                            }
                        })
                .build();
        return okClient;

    }

}
