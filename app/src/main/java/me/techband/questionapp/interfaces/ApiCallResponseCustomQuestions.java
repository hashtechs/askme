package me.techband.questionapp.interfaces;

import me.techband.questionapp.models.QuestionModel;

/**
 * Created by moayed on 9/21/17.
 */

public interface ApiCallResponseCustomQuestions {

    void onSuccess(QuestionModel[] responseObject, String responseMessage);

    void onFailure(String errorResponse);

}

