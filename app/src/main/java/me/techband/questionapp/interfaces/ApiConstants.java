package me.techband.questionapp.interfaces;

/**
 * Created by moayed on 6/14/18.
 */

public interface ApiConstants {


    String Login = "user/login";
    String SignUp = "user/register";
    String MYQUESTIONS = "my_questions";
    String ALLPOSTS = "comments_allowed_public_questions";
    String ADDQUESTION = "node";
    String imageUrl = "http://82.212.85.109/";
    String GETALLITEMTYPE = "GetAllItemType";


}
