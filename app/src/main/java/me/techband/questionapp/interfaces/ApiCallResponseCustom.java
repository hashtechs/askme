package me.techband.questionapp.interfaces;

import me.techband.questionapp.models.QuestionModel;

/**
 * Created by moayed on 9/21/17.
 */

public interface ApiCallResponseCustom {

    void onSuccess(Object[] responseObject, String responseMessage);

    void onFailure(String errorResponse);

}

