package me.techband.questionapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddQuestionModel {


    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("type")
    @Expose
    private String type;

    public AddQuestionModel(String title, String type, FieldQuestion fieldQuestion, FieldQuestionerName fieldQuestionerName, FieldQuestionerCountry fieldQuestionerCountry, FieldQuestionerCity fieldQuestionerCity, FieldQuestionerAge fieldQuestionerAge, FieldQuestionerSex fieldQuestionerSex, FieldQuestionAnswer fieldQuestionAnswer, FieldQuestionType fieldQuestionType, FieldUid fieldUid, FieldCommentsAllowed fieldCommentsAllowed, FieldIsPublic fieldIsPublic) {
        this.title = title;
        this.type = type;
        this.fieldQuestion = fieldQuestion;
        this.fieldQuestionerName = fieldQuestionerName;
        this.fieldQuestionerCountry = fieldQuestionerCountry;
        this.fieldQuestionerCity = fieldQuestionerCity;
        this.fieldQuestionerAge = fieldQuestionerAge;
        this.fieldQuestionerSex = fieldQuestionerSex;
        this.fieldQuestionAnswer = fieldQuestionAnswer;
        this.fieldQuestionType = fieldQuestionType;
        this.fieldUid = fieldUid;
        this.fieldCommentsAllowed = fieldCommentsAllowed;
        this.fieldIsPublic = fieldIsPublic;
    }

    @SerializedName("field_question")
    @Expose
    private FieldQuestion fieldQuestion;
    @SerializedName("field_questioner_name")
    @Expose
    private FieldQuestionerName fieldQuestionerName;
    @SerializedName("field_questioner_country")
    @Expose
    private FieldQuestionerCountry fieldQuestionerCountry;
    @SerializedName("field_questioner_city")
    @Expose
    private FieldQuestionerCity fieldQuestionerCity;
    @SerializedName("field_questioner_age")
    @Expose
    private FieldQuestionerAge fieldQuestionerAge;
    @SerializedName("field_questioner_sex")
    @Expose
    private FieldQuestionerSex fieldQuestionerSex;
    @SerializedName("field_question_answer")
    @Expose
    private FieldQuestionAnswer fieldQuestionAnswer;
    @SerializedName("field_question_type")
    @Expose
    private FieldQuestionType fieldQuestionType;
    @SerializedName("field_uid")
    @Expose
    private FieldUid fieldUid;
    @SerializedName("field_comments_allowed")
    @Expose
    private FieldCommentsAllowed fieldCommentsAllowed;
    @SerializedName("field_is_public")
    @Expose
    private FieldIsPublic fieldIsPublic;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public FieldQuestion getFieldQuestion() {
        return fieldQuestion;
    }

    public void setFieldQuestion(FieldQuestion fieldQuestion) {
        this.fieldQuestion = fieldQuestion;
    }

    public FieldQuestionerName getFieldQuestionerName() {
        return fieldQuestionerName;
    }

    public void setFieldQuestionerName(FieldQuestionerName fieldQuestionerName) {
        this.fieldQuestionerName = fieldQuestionerName;
    }

    public FieldQuestionerCountry getFieldQuestionerCountry() {
        return fieldQuestionerCountry;
    }

    public void setFieldQuestionerCountry(FieldQuestionerCountry fieldQuestionerCountry) {
        this.fieldQuestionerCountry = fieldQuestionerCountry;
    }

    public FieldQuestionerCity getFieldQuestionerCity() {
        return fieldQuestionerCity;
    }

    public void setFieldQuestionerCity(FieldQuestionerCity fieldQuestionerCity) {
        this.fieldQuestionerCity = fieldQuestionerCity;
    }

    public FieldQuestionerAge getFieldQuestionerAge() {
        return fieldQuestionerAge;
    }

    public void setFieldQuestionerAge(FieldQuestionerAge fieldQuestionerAge) {
        this.fieldQuestionerAge = fieldQuestionerAge;
    }

    public FieldQuestionerSex getFieldQuestionerSex() {
        return fieldQuestionerSex;
    }

    public void setFieldQuestionerSex(FieldQuestionerSex fieldQuestionerSex) {
        this.fieldQuestionerSex = fieldQuestionerSex;
    }

    public FieldQuestionAnswer getFieldQuestionAnswer() {
        return fieldQuestionAnswer;
    }

    public void setFieldQuestionAnswer(FieldQuestionAnswer fieldQuestionAnswer) {
        this.fieldQuestionAnswer = fieldQuestionAnswer;
    }

    public FieldQuestionType getFieldQuestionType() {
        return fieldQuestionType;
    }

    public void setFieldQuestionType(FieldQuestionType fieldQuestionType) {
        this.fieldQuestionType = fieldQuestionType;
    }

    public FieldUid getFieldUid() {
        return fieldUid;
    }

    public void setFieldUid(FieldUid fieldUid) {
        this.fieldUid = fieldUid;
    }

    public FieldCommentsAllowed getFieldCommentsAllowed() {
        return fieldCommentsAllowed;
    }

    public void setFieldCommentsAllowed(FieldCommentsAllowed fieldCommentsAllowed) {
        this.fieldCommentsAllowed = fieldCommentsAllowed;
    }

    public FieldIsPublic getFieldIsPublic() {
        return fieldIsPublic;
    }

    public void setFieldIsPublic(FieldIsPublic fieldIsPublic) {
        this.fieldIsPublic = fieldIsPublic;
    }


    public static class FieldCommentsAllowed {

        @SerializedName("und")
        @Expose
        private List<Integer> und = null;

        public List<Integer> getUnd() {
            return und;
        }

        public void setUnd(List<Integer> und) {
            this.und = und;
        }

    }


    public static class FieldIsPublic {

        @SerializedName("und")
        @Expose
        private List<Integer> und = null;

        public List<Integer> getUnd() {
            return und;
        }

        public void setUnd(List<Integer> und) {
            this.und = und;
        }

    }


    public static class FieldQuestion {

        @SerializedName("und")
        @Expose
        private List<Und> und = null;

        public List<Und> getUnd() {
            return und;
        }

        public void setUnd(List<Und> und) {
            this.und = und;
        }

    }


    public static class FieldQuestionAnswer {

        @SerializedName("und")
        @Expose
        private List<Und______> und = null;

        public List<Und______> getUnd() {
            return und;
        }

        public void setUnd(List<Und______> und) {
            this.und = und;
        }

    }


    public static class FieldQuestionType {

        @SerializedName("und")
        @Expose
        private List<Und_______> und = null;

        public List<Und_______> getUnd() {
            return und;
        }

        public void setUnd(List<Und_______> und) {
            this.und = und;
        }

    }


    public static class FieldQuestionerAge {

        @SerializedName("und")
        @Expose
        private List<Und____> und = null;

        public List<Und____> getUnd() {
            return und;
        }

        public void setUnd(List<Und____> und) {
            this.und = und;
        }

    }


    public static class FieldQuestionerCity {

        @SerializedName("und")
        @Expose
        private List<Und___> und = null;

        public List<Und___> getUnd() {
            return und;
        }

        public void setUnd(List<Und___> und) {
            this.und = und;
        }

    }

    public static class FieldQuestionerCountry {

        @SerializedName("und")
        @Expose
        private List<Und__> und = null;

        public List<Und__> getUnd() {
            return und;
        }

        public void setUnd(List<Und__> und) {
            this.und = und;
        }

    }


    public static class FieldQuestionerName {

        @SerializedName("und")
        @Expose
        private List<Und_> und = null;

        public List<Und_> getUnd() {
            return und;
        }

        public void setUnd(List<Und_> und) {
            this.und = und;
        }

    }

    public static class FieldQuestionerSex {

        @SerializedName("und")
        @Expose
        private List<Und_____> und = null;

        public List<Und_____> getUnd() {
            return und;
        }

        public void setUnd(List<Und_____> und) {
            this.und = und;
        }

    }


    public static class FieldUid {

        @SerializedName("und")
        @Expose
        private List<Integer> und = null;

        public List<Integer> getUnd() {
            return und;
        }

        public void setUnd(List<Integer> und) {
            this.und = und;
        }

    }

    public static class Und {

        @SerializedName("value")
        @Expose
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }


    public static class Und_ {

        @SerializedName("value")
        @Expose
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }


    public static class Und__ {

        @SerializedName("value")
        @Expose
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }


    public static class Und___ {

        @SerializedName("value")
        @Expose
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }


    public static class Und____ {

        @SerializedName("value")
        @Expose
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }


    public static class Und_____ {

        @SerializedName("value")
        @Expose
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }


    public static class Und______ {

        @SerializedName("value")
        @Expose
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }

    public static class Und_______ {

        @SerializedName("value")
        @Expose
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }

}
