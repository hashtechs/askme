package me.techband.questionapp.models;

public class RegisterUserModel {

    String name ,mail,pass,field_gender,field_nationality,field_full_name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getField_gender() {
        return field_gender;
    }

    public void setField_gender(String field_gender) {
        this.field_gender = field_gender;
    }

    public String getField_nationality() {
        return field_nationality;
    }

    public void setField_nationality(String field_nationality) {
        this.field_nationality = field_nationality;
    }

    public String getField_full_name() {
        return field_full_name;
    }

    public RegisterUserModel(String name, String mail, String pass, String field_gender, String field_nationality, String field_full_name) {
        this.name = name;
        this.mail = mail;
        this.pass = pass;
        this.field_gender = field_gender;
        this.field_nationality = field_nationality;
        this.field_full_name = field_full_name;
    }

    public void setField_full_name(String field_full_name) {
        this.field_full_name = field_full_name;
    }
}
