package me.techband.questionapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddCommentModel {


    public AddCommentModel() {
    }

    public static class CommentBody {

        @SerializedName("und")
        @Expose
        private List<Und> und = null;

        public List<Und> getUnd() {
            return und;
        }

        public void setUnd(List<Und> und) {
            this.und = und;
        }

    }

    public AddCommentModel(String nid, String subject, String uid, CommentBody commentBody) {
        this.nid = nid;
        this.subject = subject;
        this.uid = uid;
        this.commentBody = commentBody;
    }

    @SerializedName("nid")
        @Expose
        private String nid;
        @SerializedName("subject")
        @Expose
        private String subject;
        @SerializedName("uid")
        @Expose
        private String uid;
        @SerializedName("comment_body")
        @Expose
        private CommentBody commentBody;

        public String getNid() {
            return nid;
        }

        public void setNid(String nid) {
            this.nid = nid;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public CommentBody getCommentBody() {
            return commentBody;
        }

        public void setCommentBody(CommentBody commentBody) {
            this.commentBody = commentBody;
        }



    public static class Und {

        @SerializedName("value")
        @Expose
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        }
}
