package me.techband.questionapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RefreshTokenModel {
    @SerializedName("uid")
    @Expose
    private String uid;
    @SerializedName("current_pass")
    @Expose
    private String currentPass;
    @SerializedName("field_firebase_token")
    @Expose
    private FieldFirebaseToken fieldFirebaseToken;
    @SerializedName("field_os_platform")
    @Expose
    private FieldOsPlatform fieldOsPlatform;

    public String getUid() {
        return uid;
    }

    public RefreshTokenModel(String uid, String currentPass, FieldFirebaseToken fieldFirebaseToken, FieldOsPlatform fieldOsPlatform) {
        this.uid = uid;
        this.currentPass = currentPass;
        this.fieldFirebaseToken = fieldFirebaseToken;
        this.fieldOsPlatform = fieldOsPlatform;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getCurrentPass() {
        return currentPass;
    }

    public void setCurrentPass(String currentPass) {
        this.currentPass = currentPass;
    }

    public FieldFirebaseToken getFieldFirebaseToken() {
        return fieldFirebaseToken;
    }

    public void setFieldFirebaseToken(FieldFirebaseToken fieldFirebaseToken) {
        this.fieldFirebaseToken = fieldFirebaseToken;
    }

    public FieldOsPlatform getFieldOsPlatform() {
        return fieldOsPlatform;
    }

    public void setFieldOsPlatform(FieldOsPlatform fieldOsPlatform) {
        this.fieldOsPlatform = fieldOsPlatform;
    }


public static class FieldFirebaseToken {

    @SerializedName("und")
    @Expose
    private List<Und> und = null;

    public List<Und> getUnd() {
        return und;
    }

    public void setUnd(List<Und> und) {
        this.und = und;
    }

}
public static class FieldOsPlatform {

    @SerializedName("und")
    @Expose
    private List<Und_> und = null;

    public List<Und_> getUnd() {
        return und;
    }

    public void setUnd(List<Und_> und) {
        this.und = und;
    }

}

public static class Und {

    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("format")
    @Expose
    private Object format;
    @SerializedName("safe_value")
    @Expose
    private String safeValue;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Object getFormat() {
        return format;
    }

    public void setFormat(Object format) {
        this.format = format;
    }

    public String getSafeValue() {
        return safeValue;
    }

    public void setSafeValue(String safeValue) {
        this.safeValue = safeValue;
    }

}

public static class Und_ {

    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("format")
    @Expose
    private Object format;
    @SerializedName("safe_value")
    @Expose
    private String safeValue;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Object getFormat() {
        return format;
    }

    public void setFormat(Object format) {
        this.format = format;
    }

    public String getSafeValue() {
        return safeValue;
    }

    public void setSafeValue(String safeValue) {
        this.safeValue = safeValue;
    }
}
}
