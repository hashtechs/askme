package me.techband.questionapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Array;
import java.util.List;

public class QuestionModel{


    @SerializedName("question")
    @Expose
    private Object question;

    @SerializedName("question_answer")
    @Expose
    private Object questionAnswer;
    @SerializedName("question_type")
    @Expose
    private Object questionType;
    @SerializedName("questioner_age")
    @Expose
    private Object questionerAge;
    @SerializedName("questioner_city")
    @Expose
    private Object questionerCity;
    @SerializedName("questioner_country")
    @Expose
    private Object questionerCountry;
    @SerializedName("questioner_name")
    @Expose
    private Object questionerName;
    @SerializedName("questioner_sex")
    @Expose
    private Object questionerSex;
    @SerializedName("comments_allowed")
    @Expose
    private Object commentsAllowed;
    @SerializedName("question_id")
    @Expose
    private Object questionId;
    @SerializedName("question_date")
    @Expose
    private Object questionDate;
    @SerializedName("is_public")
    @Expose
    private Object isPublic;

    public Object getQuestion() {
        return question;
    }

    public void setQuestion(Object question) {
        this.question = question;
    }

    public Object getQuestionAnswer() {
        return questionAnswer;
    }

    public void setQuestionAnswer(String[] questionAnswer) {
        this.questionAnswer = questionAnswer;
    }

    public Object getQuestionType() {
        return questionType;
    }

    public void setQuestionType(Object questionType) {
        this.questionType = questionType;
    }

    public Object getQuestionerAge() {
        return questionerAge;
    }

    public void setQuestionerAge(Object questionerAge) {
        this.questionerAge = questionerAge;
    }

    public Object getQuestionerCity() {
        return questionerCity;
    }

    public void setQuestionerCity(Object questionerCity) {
        this.questionerCity = questionerCity;
    }

    public Object getQuestionerCountry() {
        return questionerCountry;
    }

    public void setQuestionerCountry(Object questionerCountry) {
        this.questionerCountry = questionerCountry;
    }

    public Object getQuestionerName() {
        return questionerName;
    }

    public void setQuestionerName(Object questionerName) {
        this.questionerName = questionerName;
    }

    public Object getQuestionerSex() {
        return questionerSex;
    }

    public void setQuestionerSex(Object questionerSex) {
        this.questionerSex = questionerSex;
    }

    public Object getCommentsAllowed() {
        return commentsAllowed;
    }

    public void setCommentsAllowed(Object commentsAllowed) {
        this.commentsAllowed = commentsAllowed;
    }

    public Object getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Object questionId) {
        this.questionId = questionId;
    }

    public Object getQuestionDate() {
        return questionDate;
    }

    public void setQuestionDate(Object questionDate) {
        this.questionDate = questionDate;
    }

    public Object getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(Object isPublic) {
        this.isPublic = isPublic;
    }
}

