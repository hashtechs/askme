package me.techband.questionapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ContactModel {


    public ContactModel(String title, String type, FieldUserName fieldUserName, FieldEmailAddress fieldEmailAddress, FieldContactUsPhoneNum fieldContactUsPhoneNum, FieldDeviceType fieldDeviceType, FieldBody fieldBody) {
        this.title = title;
        this.type = type;
        this.fieldUserName = fieldUserName;
        this.fieldEmailAddress = fieldEmailAddress;
        this.fieldContactUsPhoneNum = fieldContactUsPhoneNum;
        this.fieldDeviceType = fieldDeviceType;
        this.fieldBody = fieldBody;
    }

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("field_user_name")
    @Expose
    private FieldUserName fieldUserName;
    @SerializedName("field_email_address")
    @Expose
    private FieldEmailAddress fieldEmailAddress;
    @SerializedName("field_contact_us_phone_num")
    @Expose
    private FieldContactUsPhoneNum fieldContactUsPhoneNum;
    @SerializedName("field_device_type")
    @Expose
    private FieldDeviceType fieldDeviceType;
    @SerializedName("field_body")
    @Expose
    private FieldBody fieldBody;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public FieldUserName getFieldUserName() {
        return fieldUserName;
    }

    public void setFieldUserName(FieldUserName fieldUserName) {
        this.fieldUserName = fieldUserName;
    }

    public FieldEmailAddress getFieldEmailAddress() {
        return fieldEmailAddress;
    }

    public void setFieldEmailAddress(FieldEmailAddress fieldEmailAddress) {
        this.fieldEmailAddress = fieldEmailAddress;
    }

    public FieldContactUsPhoneNum getFieldContactUsPhoneNum() {
        return fieldContactUsPhoneNum;
    }

    public void setFieldContactUsPhoneNum(FieldContactUsPhoneNum fieldContactUsPhoneNum) {
        this.fieldContactUsPhoneNum = fieldContactUsPhoneNum;
    }

    public FieldDeviceType getFieldDeviceType() {
        return fieldDeviceType;
    }

    public void setFieldDeviceType(FieldDeviceType fieldDeviceType) {
        this.fieldDeviceType = fieldDeviceType;
    }

    public FieldBody getFieldBody() {
        return fieldBody;
    }

    public void setFieldBody(FieldBody fieldBody) {
        this.fieldBody = fieldBody;
    }


    public static class FieldBody {

        @SerializedName("und")
        @Expose
        private List<Und____> und = null;

        public List<Und____> getUnd() {
            return und;
        }

        public void setUnd(List<Und____> und) {
            this.und = und;
        }

    }


    public static class FieldContactUsPhoneNum {

        @SerializedName("und")
        @Expose
        private List<Und__> und = null;

        public List<Und__> getUnd() {
            return und;
        }

        public void setUnd(List<Und__> und) {
            this.und = und;
        }

    }


    public static class FieldDeviceType {

        @SerializedName("und")
        @Expose
        private List<Und___> und = null;

        public List<Und___> getUnd() {
            return und;
        }

        public void setUnd(List<Und___> und) {
            this.und = und;
        }

    }


    public static class FieldEmailAddress {

        @SerializedName("und")
        @Expose
        private List<Und_> und = null;

        public List<Und_> getUnd() {
            return und;
        }

        public void setUnd(List<Und_> und) {
            this.und = und;
        }

    }


    public static class FieldUserName {

        @SerializedName("und")
        @Expose
        private List<Und> und = null;

        public List<Und> getUnd() {
            return und;
        }

        public void setUnd(List<Und> und) {
            this.und = und;
        }

    }


    public static class Und {

        @SerializedName("value")
        @Expose
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }


    public static class Und_ {

        @SerializedName("value")
        @Expose
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }

    public static class Und__ {

        @SerializedName("value")
        @Expose
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }


    public static class Und___ {

        @SerializedName("value")
        @Expose
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }

    public static class Und____ {

        @SerializedName("value")
        @Expose
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
