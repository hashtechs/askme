package me.techband.questionapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CommentModel {

    public class Changed {

        @SerializedName("predicates")
        @Expose
        private List<String> predicates = null;
        @SerializedName("datatype")
        @Expose
        private String datatype;
        @SerializedName("callback")
        @Expose
        private String callback;

        public List<String> getPredicates() {
            return predicates;
        }

        public void setPredicates(List<String> predicates) {
            this.predicates = predicates;
        }

        public String getDatatype() {
            return datatype;
        }

        public void setDatatype(String datatype) {
            this.datatype = datatype;
        }

        public String getCallback() {
            return callback;
        }

        public void setCallback(String callback) {
            this.callback = callback;
        }

    }

    public class CommentBody {

        @SerializedName("und")
        @Expose
        private List<Und> und = null;

        public List<Und> getUnd() {
            return und;
        }

        public void setUnd(List<Und> und) {
            this.und = und;
        }

    }

    public class CommentBody_ {

        @SerializedName("predicates")
        @Expose
        private List<String> predicates = null;

        public List<String> getPredicates() {
            return predicates;
        }

        public void setPredicates(List<String> predicates) {
            this.predicates = predicates;
        }

    }

    public class Created {

        @SerializedName("predicates")
        @Expose
        private List<String> predicates = null;
        @SerializedName("datatype")
        @Expose
        private String datatype;
        @SerializedName("callback")
        @Expose
        private String callback;

        public List<String> getPredicates() {
            return predicates;
        }

        public void setPredicates(List<String> predicates) {
            this.predicates = predicates;
        }

        public String getDatatype() {
            return datatype;
        }

        public void setDatatype(String datatype) {
            this.datatype = datatype;
        }

        public String getCallback() {
            return callback;
        }

        public void setCallback(String callback) {
            this.callback = callback;
        }

    }

    public class Date {

        @SerializedName("property")
        @Expose
        private List<String> property = null;
        @SerializedName("content")
        @Expose
        private String content;
        @SerializedName("datatype")
        @Expose
        private String datatype;

        public List<String> getProperty() {
            return property;
        }

        public void setProperty(List<String> property) {
            this.property = property;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getDatatype() {
            return datatype;
        }

        public void setDatatype(String datatype) {
            this.datatype = datatype;
        }

    }


        @SerializedName("cid")
        @Expose
        private String cid;
        @SerializedName("pid")
        @Expose
        private String pid;
        @SerializedName("nid")
        @Expose
        private String nid;
        @SerializedName("uid")
        @Expose
        private String uid;
        @SerializedName("subject")
        @Expose
        private String subject;
        @SerializedName("hostname")
        @Expose
        private String hostname;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("changed")
        @Expose
        private String changed;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("thread")
        @Expose
        private String thread;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("mail")
        @Expose
        private String mail;
        @SerializedName("homepage")
        @Expose
        private String homepage;
        @SerializedName("language")
        @Expose
        private String language;
        @SerializedName("node_type")
        @Expose
        private String nodeType;
        @SerializedName("registered_name")
        @Expose
        private String registeredName;
        @SerializedName("u_uid")
        @Expose
        private String uUid;
        @SerializedName("signature")
        @Expose
        private String signature;
        @SerializedName("signature_format")
        @Expose
        private Object signatureFormat;
        @SerializedName("picture")
        @Expose
        private String picture;
        @SerializedName("new")
        @Expose
        private Integer _new;
        @SerializedName("comment_body")
        @Expose
        private CommentBody commentBody;
        @SerializedName("rdf_mapping")
        @Expose
        private RdfMapping rdfMapping;
        @SerializedName("rdf_data")
        @Expose
        private RdfData rdfData;

        public String getCid() {
            return cid;
        }

        public void setCid(String cid) {
            this.cid = cid;
        }

        public String getPid() {
            return pid;
        }

        public void setPid(String pid) {
            this.pid = pid;
        }

        public String getNid() {
            return nid;
        }

        public void setNid(String nid) {
            this.nid = nid;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getHostname() {
            return hostname;
        }

        public void setHostname(String hostname) {
            this.hostname = hostname;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getChanged() {
            return changed;
        }

        public void setChanged(String changed) {
            this.changed = changed;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getThread() {
            return thread;
        }

        public void setThread(String thread) {
            this.thread = thread;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMail() {
            return mail;
        }

        public void setMail(String mail) {
            this.mail = mail;
        }

        public String getHomepage() {
            return homepage;
        }

        public void setHomepage(String homepage) {
            this.homepage = homepage;
        }

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getNodeType() {
            return nodeType;
        }

        public void setNodeType(String nodeType) {
            this.nodeType = nodeType;
        }

        public String getRegisteredName() {
            return registeredName;
        }

        public void setRegisteredName(String registeredName) {
            this.registeredName = registeredName;
        }

        public String getUUid() {
            return uUid;
        }

        public void setUUid(String uUid) {
            this.uUid = uUid;
        }

        public String getSignature() {
            return signature;
        }

        public void setSignature(String signature) {
            this.signature = signature;
        }

        public Object getSignatureFormat() {
            return signatureFormat;
        }

        public void setSignatureFormat(Object signatureFormat) {
            this.signatureFormat = signatureFormat;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }

        public Integer getNew() {
            return _new;
        }

        public void setNew(Integer _new) {
            this._new = _new;
        }

        public CommentBody getCommentBody() {
            return commentBody;
        }

        public void setCommentBody(CommentBody commentBody) {
            this.commentBody = commentBody;
        }

        public RdfMapping getRdfMapping() {
            return rdfMapping;
        }

        public void setRdfMapping(RdfMapping rdfMapping) {
            this.rdfMapping = rdfMapping;
        }

        public RdfData getRdfData() {
            return rdfData;
        }

        public void setRdfData(RdfData rdfData) {
            this.rdfData = rdfData;
        }


    public class Name {

        @SerializedName("predicates")
        @Expose
        private List<String> predicates = null;

        public List<String> getPredicates() {
            return predicates;
        }

        public void setPredicates(List<String> predicates) {
            this.predicates = predicates;
        }

    }

    public class Pid {

        @SerializedName("predicates")
        @Expose
        private List<String> predicates = null;
        @SerializedName("type")
        @Expose
        private String type;

        public List<String> getPredicates() {
            return predicates;
        }

        public void setPredicates(List<String> predicates) {
            this.predicates = predicates;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

    }

    public class RdfData {

        @SerializedName("date")
        @Expose
        private Date date;
        @SerializedName("nid_uri")
        @Expose
        private String nidUri;

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public String getNidUri() {
            return nidUri;
        }

        public void setNidUri(String nidUri) {
            this.nidUri = nidUri;
        }

    }

    public class RdfMapping {

        @SerializedName("rdftype")
        @Expose
        private List<String> rdftype = null;
        @SerializedName("title")
        @Expose
        private Title title;
        @SerializedName("created")
        @Expose
        private Created created;
        @SerializedName("changed")
        @Expose
        private Changed changed;
        @SerializedName("comment_body")
        @Expose
        private CommentBody_ commentBody;
        @SerializedName("pid")
        @Expose
        private Pid pid;
        @SerializedName("uid")
        @Expose
        private Uid uid;
        @SerializedName("name")
        @Expose
        private Name name;

        public List<String> getRdftype() {
            return rdftype;
        }

        public void setRdftype(List<String> rdftype) {
            this.rdftype = rdftype;
        }

        public Title getTitle() {
            return title;
        }

        public void setTitle(Title title) {
            this.title = title;
        }

        public Created getCreated() {
            return created;
        }

        public void setCreated(Created created) {
            this.created = created;
        }

        public Changed getChanged() {
            return changed;
        }

        public void setChanged(Changed changed) {
            this.changed = changed;
        }

        public CommentBody_ getCommentBody() {
            return commentBody;
        }

        public void setCommentBody(CommentBody_ commentBody) {
            this.commentBody = commentBody;
        }

        public Pid getPid() {
            return pid;
        }

        public void setPid(Pid pid) {
            this.pid = pid;
        }

        public Uid getUid() {
            return uid;
        }

        public void setUid(Uid uid) {
            this.uid = uid;
        }

        public Name getName() {
            return name;
        }

        public void setName(Name name) {
            this.name = name;
        }

    }

    public class Title {

        @SerializedName("predicates")
        @Expose
        private List<String> predicates = null;

        public List<String> getPredicates() {
            return predicates;
        }

        public void setPredicates(List<String> predicates) {
            this.predicates = predicates;
        }

    }


    public class Uid {

        @SerializedName("predicates")
        @Expose
        private List<String> predicates = null;
        @SerializedName("type")
        @Expose
        private String type;

        public List<String> getPredicates() {
            return predicates;
        }

        public void setPredicates(List<String> predicates) {
            this.predicates = predicates;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

    }

    public class Und {

        @SerializedName("value")
        @Expose
        private String value;
        @SerializedName("format")
        @Expose
        private String format;
        @SerializedName("safe_value")
        @Expose
        private String safeValue;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getFormat() {
            return format;
        }

        public void setFormat(String format) {
            this.format = format;
        }

        public String getSafeValue() {
            return safeValue;
        }

        public void setSafeValue(String safeValue) {
            this.safeValue = safeValue;
        }

    }





}