package me.techband.questionapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UpdateModel {


    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("mail")
    @Expose
    private String mail;

    @SerializedName("current_pass")
    @Expose
    private String currentPass;


    @SerializedName("pass")
    @Expose
    private String pass;


    @SerializedName("field_gender")
    @Expose
    private FieldGender fieldGender;
    @SerializedName("field_nationality")
    @Expose
    private FieldNationality fieldNationality;
    @SerializedName("field_full_name")
    @Expose
    private FieldFullName fieldFullName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    private String id;

    public UpdateModel(String name, String mail, FieldGender fieldGender, FieldNationality fieldNationality, FieldFullName fieldFullName) {
        this.name = name;
        this.mail = mail;
        this.fieldGender = fieldGender;
        this.fieldNationality = fieldNationality;
        this.fieldFullName = fieldFullName;
    }

    public UpdateModel(String currentPass, String pass, String id) {
        this.currentPass = currentPass;
        this.pass = pass;
        this.id = id;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public FieldGender getFieldGender() {
        return fieldGender;
    }

    public void setFieldGender(FieldGender fieldGender) {
        this.fieldGender = fieldGender;
    }

    public FieldNationality getFieldNationality() {
        return fieldNationality;
    }

    public void setFieldNationality(FieldNationality fieldNationality) {
        this.fieldNationality = fieldNationality;
    }

    public FieldFullName getFieldFullName() {
        return fieldFullName;
    }

    public void setFieldFullName(FieldFullName fieldFullName) {
        this.fieldFullName = fieldFullName;
    }

    public String getCurrentPass() {
        return currentPass;
    }

    public void setCurrentPass(String currentPass) {
        this.currentPass = currentPass;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public static class FieldFullName {

        @SerializedName("und")
        @Expose
        private List<Und__> und = null;

        public List<Und__> getUnd() {
            return und;
        }

        public void setUnd(List<Und__> und) {
            this.und = und;
        }

    }


    public static class FieldGender {

        @SerializedName("und")
        @Expose
        private List<Und> und = null;

        public List<Und> getUnd() {
            return und;
        }

        public void setUnd(List<Und> und) {
            this.und = und;
        }

    }


    public static class FieldNationality {

        @SerializedName("und")
        @Expose
        private List<Und_> und = null;

        public List<Und_> getUnd() {
            return und;
        }

        public void setUnd(List<Und_> und) {
            this.und = und;
        }

    }


    public static class Und {

        @SerializedName("value")
        @Expose
        private String value;
        @SerializedName("format")
        @Expose
        private Object format;
        @SerializedName("safe_value")
        @Expose
        private String safeValue;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public Object getFormat() {
            return format;
        }

        public void setFormat(Object format) {
            this.format = format;
        }

        public String getSafeValue() {
            return safeValue;
        }

        public void setSafeValue(String safeValue) {
            this.safeValue = safeValue;
        }

    }


    public static class Und_ {

        @SerializedName("value")
        @Expose
        private String value;
        @SerializedName("format")
        @Expose
        private Object format;
        @SerializedName("safe_value")
        @Expose
        private String safeValue;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public Object getFormat() {
            return format;
        }

        public void setFormat(Object format) {
            this.format = format;
        }

        public String getSafeValue() {
            return safeValue;
        }

        public void setSafeValue(String safeValue) {
            this.safeValue = safeValue;
        }

    }

    public static class Und__ {

        @SerializedName("value")
        @Expose
        private String value;
        @SerializedName("format")
        @Expose
        private Object format;
        @SerializedName("safe_value")
        @Expose
        private String safeValue;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public Object getFormat() {
            return format;
        }

        public void setFormat(Object format) {
            this.format = format;
        }

        public String getSafeValue() {
            return safeValue;
        }

        public void setSafeValue(String safeValue) {
            this.safeValue = safeValue;
        }
    }
}
