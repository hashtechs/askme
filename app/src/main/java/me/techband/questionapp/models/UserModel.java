package me.techband.questionapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserModel {


        @SerializedName("uid")
        @Expose
        private String uid;
        @SerializedName("name")
        @Expose
        private String name;

        @SerializedName("mail")
        @Expose
        private String mail;

        @SerializedName("full_name")
        @Expose
        private String full_name;


        @SerializedName("user_gender")
        @Expose
        private String user_gender;


        @SerializedName("nationality")
        @Expose
        private String nationality;


        @SerializedName("ustatus")
        @Expose
        private String ustatus;

        @SerializedName("picture")
        @Expose
        private String picture;


        @SerializedName("session_cookie")
        @Expose
        private String session_cookie;


        @SerializedName("token")
        @Expose
        private String token;

        private String password;




        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMail() {
            return mail;
        }

        public void setMail(String mail) {
            this.mail = mail;
        }

        public String getFullname() {
            return full_name;
        }

        public void setFullname(String fullname) {
            this.full_name = fullname;
        }

        public String getUser_gender() {
            return user_gender;
        }

        public void setUser_gender(String user_gender) {
            this.user_gender = user_gender;
        }

        public String getNationality() {
            return nationality;
        }

        public void setNationality(String nationality) {
            this.nationality = nationality;
        }

        public String getUstatus() {
            return ustatus;
        }

        public void setUstatus(String ustatus) {
            this.ustatus = ustatus;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }

        public String getSession_cookie() {
            return session_cookie;
        }

        public void setSession_cookie(String session_cookie) {
            this.session_cookie = session_cookie;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
