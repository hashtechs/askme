package me.techband.questionapp.ui.fragments;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import me.techband.questionapp.R;
import me.techband.questionapp.helpers.SharedPrefConstants;
import me.techband.questionapp.helpers.SharedPreferencesHelper;
import me.techband.questionapp.models.UserModel;
import me.techband.questionapp.ui.activities.MainActivity;
import me.techband.questionapp.utils.RadioButtonCustomFont;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;


public class MyQuestionsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;


    @BindView(R.id.rb_posts_with_comments)
    RadioButtonCustomFont rbPostWithCommentsTab;
    @BindView(R.id.rb_posts_tab)
    RadioButtonCustomFont rbPostsTab;


    boolean stateTabs = false;


    public MyQuestionsFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static MyQuestionsFragment newInstance(String param1, String param2) {
        MyQuestionsFragment fragment = new MyQuestionsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_questions, container, false);

        ButterKnife.bind(this, view);



        ((MainActivity) getContext()).mToolBarTextView.setText("أسئلتي");
        ((MainActivity) getContext()).imgComments.setVisibility(View.GONE);

        if(!stateTabs)
            ((MainActivity) getContext()).changeFragmentTabsMethod(new AnswerdTabFragment(), "ANSWERED",0);
        changeState(rbPostsTab, true);
        return view;
    }


    @OnCheckedChanged(R.id.rb_posts_tab)
    void onAnsweredTab(RadioButton button, boolean checked) {
        if (checked) {
            getChildFragmentManager().popBackStack();
            ((MainActivity) getContext()).changeFragmentTabsMethod(new AnswerdTabFragment(), "see All",1);

        }

        changeState(button, checked);
    }

    @OnCheckedChanged(R.id.rb_posts_with_comments)
    void onNotAnsweredTab(RadioButton button, boolean checked) {
        if (rbPostWithCommentsTab.isChecked()) {
            ((MainActivity) getContext()).changeFragmentTabsMethod(new NotAnswerdTabFragment(), "NOTANSWERED",1);
        }
        changeState(button, checked);

    }


    @OnClick(R.id.rb_posts_tab)
    void setRbPostsTab() {
        stateTabs = false;
    }

    @OnClick(R.id.rb_posts_with_comments)
    void setRbPostWithCommentsTab(){
        stateTabs = true;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    private void changeState(RadioButton button, boolean isChecked) {

        if (rbPostWithCommentsTab.isChecked()){
            rbPostWithCommentsTab.setTextColor(Color.parseColor("#FFFFFF"));
            rbPostsTab.setTextColor(Color.parseColor("#1e1e3b"));
        } else {
            rbPostsTab.setTextColor(Color.parseColor("#FFFFFF"));
            rbPostWithCommentsTab.setTextColor(Color.parseColor("#1e1e3b"));
        }
    }

//    @Override
//    public void onBackStackChanged() {
//        int count = getChildFragmentManager().getBackStackEntryCount();
//        String name;
//        if (count == 0) {
//            name = "ANSWERED";
//        } else {
//            name = getChildFragmentManager().getBackStackEntryAt(count - 1).getName();
//        }
//        if (name == null) {
//            return;
//        }
//        switch (name) {
//            case "ANSWERED":
//                rbPostsTab.setChecked(true);
//                break;
//            case "NOTANSWERED":
//                rbPostWithCommentsTab.setChecked(true);
//                break;
//        }
//    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
