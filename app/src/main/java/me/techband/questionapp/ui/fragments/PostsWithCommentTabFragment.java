package me.techband.questionapp.ui.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import me.techband.questionapp.R;
import me.techband.questionapp.adapters.MyItemAnimator;
import me.techband.questionapp.adapters.PostsAdapter;
import me.techband.questionapp.interfaces.ApiCallResponseCustom;
import me.techband.questionapp.managers.BusinessManager;
import me.techband.questionapp.models.QuestionModel;
import me.techband.questionapp.utils.PaginationScrollListener;
import me.techband.questionapp.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PostsWithCommentTabFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PostsWithCommentTabFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PostsWithCommentTabFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private boolean nearby;


    @BindView(R.id.rvPostsWithComments)
    RecyclerView rvPostsWithComments;

    LinearLayoutManager manager;


    QuestionModel[] filteredQuestion;
    private int index = 0;

    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    // limiting to 5 for this tutorial, since total pages in actual API is very large. Feel free to modify.
    private int TOTAL_PAGES = 9;
    private int currentPage = PAGE_START;

    QuestionModel[] questionModel;
    PostsAdapter adapter;





    public PostsWithCommentTabFragment() {
        // Required empty public constructor
    }


    public static PostsWithCommentTabFragment newInstance(String param1, String param2) {
        PostsWithCommentTabFragment fragment = new PostsWithCommentTabFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_posts_with_comment_tab, container, false);
        rvPostsWithComments = view.findViewById(R.id.rvPostsWithComments);


        adapter = new PostsAdapter(getContext());

        manager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvPostsWithComments.setLayoutManager(manager);

        rvPostsWithComments.setItemAnimator(new DefaultItemAnimator());

        rvPostsWithComments.setAdapter(adapter);

        rvPostsWithComments.addOnScrollListener(new PaginationScrollListener(manager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getAllPostsWithComments(currentPage,false);
                    }
                }, 1000);
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        getAllPostsWithComments(1,true);

        // Inflate the layout for this fragment
        return view;
    }


    public void getAllPostsWithComments(int page, boolean isFirst){

        if (isFirst)
            Utils.showProccessDialog(getActivity());


        new BusinessManager().getAllPosts(page,new ApiCallResponseCustom() {
            @Override
            public void onSuccess(Object[] responseObject, String responseMessage) {


                if (isFirst){

                    if (Utils.isProgressShowing()){
                        Utils.dismissProccessDialog();
                    }

                    questionModel = (QuestionModel[]) responseObject;


                   for(int i=0; i < questionModel.length; i++){
                        List<QuestionModel> list = new ArrayList<QuestionModel>(Arrays.asList(questionModel));
                        if (questionModel[i].getCommentsAllowed().equals("1")){
                            list.remove(i);
                            filteredQuestion = list.toArray(new QuestionModel[i]);
                        }
                    }

                    List<QuestionModel> results = Arrays.asList(filteredQuestion);
                    adapter.addAll(results);

                    if (currentPage <= TOTAL_PAGES) adapter.addLoadingFooter();
                    else isLastPage = true;

                } else {
                    if (Utils.isProgressShowing()){
                        Utils.dismissProccessDialog();
                    }

                    questionModel = (QuestionModel[]) responseObject;

                    for(int i=0; i < questionModel.length; i++){
                        List<QuestionModel> list = new ArrayList<QuestionModel>(Arrays.asList(questionModel));
                        if (questionModel[i].getCommentsAllowed().equals("1")){
                            list.remove(i);
                            filteredQuestion = list.toArray(new QuestionModel[i]);
                        }
                    }

                    adapter.removeLoadingFooter();
                    isLoading = false;

                    List<QuestionModel> results = Arrays.asList(filteredQuestion);
                    adapter.addAll(results);

                    if (currentPage != TOTAL_PAGES) adapter.addLoadingFooter();
                    else isLastPage = true;
                }




//                rvPostsWithComments.setLayoutManager(new LinearLayoutManager(getActivity()));
//                questionAdapter = new PostsAdapter(getActivity(), questionModel, true);
//                rvPostsWithComments.setAdapter(questionAdapter);
//
//                rvPostsWithComments.setItemAnimator(new MyItemAnimator());
//                questionAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(String errorResponse) {

            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
