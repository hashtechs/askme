package me.techband.questionapp.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import me.techband.questionapp.R;
import me.techband.questionapp.helpers.SharedPrefConstants;
import me.techband.questionapp.helpers.SharedPreferencesHelper;
import me.techband.questionapp.interfaces.ApiCallResponse;
import me.techband.questionapp.managers.BusinessManager;
import me.techband.questionapp.models.UserModel;
import me.techband.questionapp.sweetdialog.SweetAlertDialog;
import me.techband.questionapp.utils.EditTextCustomFont;
import me.techband.questionapp.utils.SpinnerUtil;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.mobsandgeeks.saripaar.annotation.Select;
import me.techband.questionapp.utils.TextviewCustomFont;
import me.techband.questionapp.utils.Utils;
import com.ybs.countrypicker.CountryPicker;
import com.ybs.countrypicker.CountryPickerListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.internal.Util;

public class RegisterActivity extends AppCompatActivity implements Validator.ValidationListener{


    @NotEmpty
    @BindView(R.id.et_username)
    EditTextCustomFont etUsername;

    @BindView(R.id.et_email)
    EditTextCustomFont etEmail;

    @BindView(R.id.et_name)
    EditTextCustomFont etFullname;

    @NotEmpty
    @Password
    @BindView(R.id.et_password)
    EditTextCustomFont etPassword;

    @BindView(R.id.et_confirm_password)
    @ConfirmPassword
    EditTextCustomFont etConfirmPassword;

    @Select
    @BindView(R.id.sp_gender)
    Spinner spGender;

    @Select
    @BindView(R.id.sp_country)
    TextviewCustomFont spCountry;


    Context context = RegisterActivity.this;

    Validator validator;

    SweetAlertDialog dialog;

    CountryPicker picker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        validator = new Validator(this);
        validator.setValidationListener(this);

        SpinnerUtil.getInstance().setupSpinner(spGender, R.array.gender_array, this);



        picker = CountryPicker.newInstance("Select Country");  // dialog title
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                spCountry.setText(name);
                picker.dismiss();

            }
        });


        spCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });
    }

    @Override
    public void onValidationSucceeded() {
        registerApiCall();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }


    @OnClick(R.id.btn_register)
    public void checkRegister(){
        registerApiCall();
    }


    private void registerApiCall() {

        String fullname = etFullname.getText().toString();
        String email =  etEmail.getText().toString();
        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();
        String confirmPassword = etConfirmPassword.getText().toString();
        String gender = spGender.getSelectedItem().toString();
        String nationalioty = spCountry.getText().toString();

        Utils.showProccessDialog(this);


        if (fullname.equals("") || email.equals("") || username.equals("") || password.equals("") || gender.equals("الجنس") || nationalioty.equals("") || password.equals("") || confirmPassword.equals("")){
            Utils.failureDialog(context, "يرجى تعبئة جميع الحقول");
            if (Utils.isProgressShowing())
                Utils.dismissProccessDialog();
        } else if (!password.equals(confirmPassword)) {
            Utils.failureDialog(context, "كلمة المرور غير مطابقة");
            if (Utils.isProgressShowing())
                Utils.dismissProccessDialog();
        } else if (Utils.isEmailValid(email)) {
            Utils.failureDialog(context, "يرجى التأكد من البريد الإلكتروني");
            if (Utils.isProgressShowing())
                Utils.dismissProccessDialog();
        } else {
            new BusinessManager().registerApiCall(this, username, email, password, gender, nationalioty, fullname, new ApiCallResponse() {
                @Override
                public void onSuccess(Object responseObject, String responseMessage) {
                    if (Utils.isProgressShowing()){
                        Utils.dismissProccessDialog();
                    }


                    if (responseMessage.contains("Not Accept")){
                        Utils.failureDialog(context, "هذا المستخدم موجود");
                    } else {
                        Gson gson = new Gson();
                        String json = responseObject.toString();
                        UserModel parseObject = gson.fromJson(json, UserModel.class);
                        parseObject.setPassword(password);
                        if (parseObject != null) {

                            if (username != null || password != null){
                                loginAPiCall(username,password);
                            }

                        } else {
                            Toast.makeText(context, "Invalid Error ", Toast.LENGTH_LONG).show();
                            Log.e("sdfsdfds", responseMessage.toString());
                            if (Utils.isProgressShowing()){
                                Utils.dismissProccessDialog();
                            }
                        }
                    }



                }

                @Override
                public void onFailure(String errorResponse) {
                    finish();
                    if (Utils.isProgressShowing()){
                        Utils.dismissProccessDialog();
                    }
                }
            });
        }

    }


    private void loginAPiCall(String username, String password, final String... user) {

        new BusinessManager().loginApiCall(context, username, password, new ApiCallResponse() {
            @Override
            public void onSuccess(Object responseObject, String responseMessage) {


                if (responseMessage.equals("Unauthorized : Wrong username or password.")) {
                    Utils.dismissProccessDialog();
                    Utils.failureDialog(context, "اسم المستخدم او كلمة المرور غير صحيح");

                } else if(username.equals("") || password.equals("")) {
                    Utils.failureDialog(context, "يرجى تعبئة جميع الحقول");
                } else {

                    UserModel loginModel = (UserModel) responseObject;
//                    loginModel.setPassword(password);
                    try {
                        if (loginModel != null) {

                            Utils.dismissProccessDialog();
                            SharedPreferencesHelper.putSharedPreferencesObject(context, SharedPrefConstants.loginObject, loginModel);
                            context.startActivity(new Intent(context, MainActivity.class));
                            finish();
                        } else {
                            Utils.dismissProccessDialog();
                            Toast.makeText(context, "error", Toast.LENGTH_LONG).show();
                        }
                    }catch (JsonSyntaxException e) {
                        Utils.dismissProccessDialog();
                        Utils.failureDialog(context, "اسم المستخدم او كلمة المرور غير صحيح");
                        e.printStackTrace();
                    }
                }


            }

            @Override
            public void onFailure(String errorResponse) {
                Toast.makeText(context, "Username or Password Incorrect ", Toast.LENGTH_LONG).show();
//                Utils.failureDialog(context, "اسم الدخول او كلمة المرور غير صحيحة");

            }
        });
    }



    public void successDialog(){
        SweetAlertDialog dialog = new SweetAlertDialog(context,
                SweetAlertDialog.SUCCESS_TYPE);
        dialog.setTitleText("success");
        dialog.setContentText("dsfdsfs");
        dialog.show();
        dialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
//                Intent intent = new Intent(context, LoginActivity.class);
//                startActivity(intent);
//                finish();
//                sweetAlertDialog.cancel();
            }
        });
    }


    private void failureDialog(String error){
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context,
                SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setTitleText("عذرا");
        sweetAlertDialog.setContentText(error);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                sweetAlertDialog.cancel();
            }
        });
        sweetAlertDialog.show();
    }
}
