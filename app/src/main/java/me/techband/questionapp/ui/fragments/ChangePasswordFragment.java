package me.techband.questionapp.ui.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import me.techband.questionapp.R;
import me.techband.questionapp.helpers.SharedPrefConstants;
import me.techband.questionapp.helpers.SharedPreferencesHelper;
import me.techband.questionapp.interfaces.ApiCallResponse;
import me.techband.questionapp.managers.BusinessManager;
import me.techband.questionapp.models.UserModel;
import me.techband.questionapp.ui.activities.MainActivity;
import me.techband.questionapp.utils.ButtonCustomFont;
import me.techband.questionapp.utils.EditTextCustomFont;
import me.techband.questionapp.utils.Utils;

import butterknife.OnClick;

public class ChangePasswordFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;





    EditTextCustomFont etCurrentPass;

    EditTextCustomFont etNewPass;

    EditTextCustomFont etConfirmPass;



    ButtonCustomFont btnSave;

    public ChangePasswordFragment() {
        // Required empty public constructor
    }


    public static ChangePasswordFragment newInstance(String param1, String param2) {
        ChangePasswordFragment fragment = new ChangePasswordFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_change_password, container, false);
        ((MainActivity) getContext()).imgComments.setVisibility(View.GONE);
        initViews(view);


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });
        return view;
    }


    public void initViews(View view){
        etCurrentPass = view.findViewById(R.id.et_current);
        etNewPass = view.findViewById(R.id.et_new_pass);
        etConfirmPass = view.findViewById(R.id.et_confirm_pass);
        btnSave = view.findViewById(R.id.btn_change_password);
    }


    public void save(){

        final String current = etCurrentPass.getText().toString();
        final String newPass = etNewPass.getText().toString();
        final String confirmPass = etConfirmPass.getText().toString();

        UserModel userLoginModel = (UserModel) SharedPreferencesHelper.getSharedPreferencesObject(getContext(), SharedPrefConstants.loginObject,UserModel.class);


        if (!userLoginModel.getPassword().equals(current)){
            Utils.failureDialog(getContext(), "يرجى التأكد من كلمة المرور الحالية");
        } else if (newPass.equals("") || confirmPass.equals("")){
            Utils.failureDialog(getContext(), "يرجى ادخال كلمة مرور");
        } else {

            if (newPass.equals(confirmPass)){
                Utils.showProccessDialog(getActivity());
                new BusinessManager().changePassword(getContext(), current, newPass, userLoginModel.getUid(), new ApiCallResponse() {
                    @Override
                    public void onSuccess(Object responseObject, String responseMessage) {
                        if (Utils.isProgressShowing()){
                            Utils.dismissProccessDialog();
                        }
                        Utils.successDialog(getContext(),"تمت تغيير كلمة المرور بنجاح");

                    }

                    @Override
                    public void onFailure(String errorResponse) {
                        if (Utils.isProgressShowing()){
                            Utils.dismissProccessDialog();
                        }
                    }
                });
            }else if(newPass.equals("") || confirmPass.equals("")){
                Utils.failureDialog(getContext(),"يرجى تعبئة جميع الحقول");
            } else {
                Utils.failureDialog(getContext(),"كلمة المرور غير مطابقة");

            }
        }


    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
