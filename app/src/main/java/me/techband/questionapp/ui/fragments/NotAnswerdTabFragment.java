package me.techband.questionapp.ui.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import me.techband.questionapp.R;
import me.techband.questionapp.adapters.MyItemAnimator;
import me.techband.questionapp.adapters.QuestionAdapter;
import me.techband.questionapp.helpers.SharedPrefConstants;
import me.techband.questionapp.helpers.SharedPreferencesHelper;
import me.techband.questionapp.interfaces.ApiCallResponseCustom;
import me.techband.questionapp.managers.BusinessManager;
import me.techband.questionapp.models.QuestionModel;
import me.techband.questionapp.models.UserModel;
import me.techband.questionapp.utils.TextviewCustomFont;
import me.techband.questionapp.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;


public class NotAnswerdTabFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;


    QuestionModel[] questionModel;

    QuestionModel[] filteredQuestion;

    @BindView(R.id.rvNotAnswerdQuestions)
    RecyclerView rvNotAnswerdQuestions;

    private QuestionAdapter questionAdapter;

    TextviewCustomFont tvNoQuestions;


    private int index = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_not_answerd_tab, container, false);
        rvNotAnswerdQuestions = view.findViewById(R.id.rvNotAnswerdQuestions);
        tvNoQuestions = view.findViewById(R.id.tv_no_questions);

        getMyQuestions();

        return view;
    }


    public void getMyQuestions() {
        Utils.showProccessDialog(getActivity());

        UserModel userLoginModel = (UserModel) SharedPreferencesHelper.getSharedPreferencesObject(getContext(), SharedPrefConstants.loginObject, UserModel.class);
        new BusinessManager().getMyQuestions(getActivity(), Integer.parseInt(userLoginModel.getUid()), new ApiCallResponseCustom() {
            @Override
            public void onSuccess(Object[] responseObject, String responseMessage) {
                if (Utils.isProgressShowing()) {
                    Utils.dismissProccessDialog();
                }
                questionModel = (QuestionModel[]) responseObject;


                Utils.dismissProccessDialog();

                List<QuestionModel> listFilter = new ArrayList<>();
                for (QuestionModel question : questionModel) {
                    if (question.getQuestionAnswer() == null || question.getQuestionAnswer().toString().isEmpty()) {
                        listFilter.add(question);
                    }
                }

                if (questionModel.length == 0) {
                    tvNoQuestions.setVisibility(View.VISIBLE);
                    rvNotAnswerdQuestions.setVisibility(View.GONE);
                    Utils.dismissProccessDialog();
                } else {
                    rvNotAnswerdQuestions.setLayoutManager(new LinearLayoutManager(getActivity()));
                    questionAdapter = new QuestionAdapter(getActivity(), listFilter, false);
                    rvNotAnswerdQuestions.setAdapter(questionAdapter);

                    rvNotAnswerdQuestions.setItemAnimator(new MyItemAnimator());
                    questionAdapter.notifyDataSetChanged();
                }
        }

            @Override
            public void onFailure(String errorResponse) {
                if (Utils.isProgressShowing()) {
                    Utils.dismissProccessDialog();
                }
            }
        });
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
