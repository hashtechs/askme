package me.techband.questionapp.ui.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import me.techband.questionapp.R;
import me.techband.questionapp.ui.activities.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;


public class PostsFragment extends Fragment implements FragmentManager.OnBackStackChangedListener{


    @BindView(R.id.rb_posts_with_comments)
    AppCompatRadioButton rbPostWithCommentsTab;
    @BindView(R.id.rb_posts_tab)
    AppCompatRadioButton rbPostsTab;

    boolean stateTabs = false;


    public PostsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        getChildFragmentManager().addOnBackStackChangedListener(this);

        if(!stateTabs)
            ((MainActivity) getContext()).changeFragmentTabsMethod(new PostsTabFragment(), "POSTS",0);

        changeState(rbPostsTab, true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity) getActivity()).imgComments.setVisibility(View.GONE);
        ((MainActivity) getActivity()).mToolBarTextView.setText("المشاركات");
        return inflater.inflate(R.layout.fragment_posts, container, false);
    }

    @OnCheckedChanged(R.id.rb_posts_tab)
    void onPostsTab(RadioButton button, boolean checked) {
        if (checked) {
            getChildFragmentManager().popBackStack();
            ((MainActivity) getContext()).changeFragmentTabsMethod(new PostsTabFragment(), "POSTS",1);
        }
        changeState(button, checked);
    }

    @OnCheckedChanged(R.id.rb_posts_with_comments)
    void onPostsWithComments(RadioButton button, boolean checked) {
        if (checked) {
            getChildFragmentManager().popBackStack();
            ((MainActivity) getContext()).changeFragmentTabsMethod(new PostsWithCommentTabFragment(), "POSTSWITHCOMMENT",1);
        }
        changeState(button, checked);
    }


    @OnClick(R.id.rb_posts_tab)
    void setRbPostsTab() {
        stateTabs = false;
    }

    @OnClick(R.id.rb_posts_with_comments)
    void setRbPostWithCommentsTab(){
        stateTabs = true;
    }


    private void changeState(RadioButton button, boolean isChecked) {

        if (rbPostWithCommentsTab.isChecked()){
            rbPostWithCommentsTab.setTextColor(Color.parseColor("#FFFFFF"));
            rbPostsTab.setTextColor(Color.parseColor("#1e1e3b"));
        } else {
            rbPostsTab.setTextColor(Color.parseColor("#FFFFFF"));
            rbPostWithCommentsTab.setTextColor(Color.parseColor("#1e1e3b"));
        }
    }

    @Override
    public void onBackStackChanged() {
        int count = getChildFragmentManager().getBackStackEntryCount();
        String name;
        if (count == 0) {
            name = "POSTS";
        } else {
            name = getChildFragmentManager().getBackStackEntryAt(count - 1).getName();
        }
        if (name == null) {
            return;
        }
        switch (name) {
            case "POSTS":
                rbPostsTab.setChecked(true);
                break;
            case "POSTSWITHCOMMENT":
                rbPostsTab.setChecked(true);
                break;
        }
    }
}
