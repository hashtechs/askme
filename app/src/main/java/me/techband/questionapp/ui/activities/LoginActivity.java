package me.techband.questionapp.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import me.techband.questionapp.R;
import me.techband.questionapp.helpers.SharedPrefConstants;
import me.techband.questionapp.helpers.SharedPreferencesHelper;
import me.techband.questionapp.interfaces.ApiCallResponse;
import me.techband.questionapp.managers.BusinessManager;
import me.techband.questionapp.models.UserModel;
import me.techband.questionapp.utils.ButtonCustomFont;
import me.techband.questionapp.utils.EditTextCustomFont;
import me.techband.questionapp.utils.Utils;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements Validator.ValidationListener{


    @BindView(R.id.logo)
    ImageView imageView_logo;

    Context context = this;

    @NotEmpty
    @BindView(R.id.et_username)
    EditTextCustomFont etUsername;

    @NotEmpty
    @BindView(R.id.et_password)
    EditTextCustomFont etPassword;

    @BindView(R.id.line)
    LinearLayout linearLayout;

    @BindView(R.id.relative_face)
    LinearLayout relativeFace;


    Validator validator;

    Bundle bundle;


    CallbackManager callbackManager;

    LoginButton login_button;


    @BindView(R.id.btn_login)
    ButtonCustomFont btnLogin;

    @BindView(R.id.btnLoginWithAK)
    ButtonCustomFont btnLoginWithAK;

    public static final int REQUEST_CODE = 999;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        validator = new Validator(this);
        validator.setValidationListener(this);
        AccountKit.initialize(getApplicationContext());

        bundle = getIntent().getExtras();

        TranslateAnimation anim = new TranslateAnimation(
                TranslateAnimation.RELATIVE_TO_SELF, 0f,
                TranslateAnimation.RELATIVE_TO_SELF, 0f,
                TranslateAnimation.RELATIVE_TO_SELF, 0f,
                TranslateAnimation.RELATIVE_TO_SELF, -1.0f);
        anim.setDuration(1500);
        anim.cancel();
        anim.setFillAfter(true);
        imageView_logo.startAnimation(anim);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Utils.isLogin(context))
                {
                    UserModel userLoginModel = (UserModel) SharedPreferencesHelper.getSharedPreferencesObject(getApplicationContext(), SharedPrefConstants.loginObject,UserModel.class);

                    loginAPiCall(userLoginModel.getName(), userLoginModel.getPassword());
                }else {
                    linearLayout.setVisibility(View.VISIBLE);
                    relativeFace.setVisibility(View.VISIBLE);
                }
            }

        }, 2000);


        callbackManager = CallbackManager.Factory.create();

        login_button = findViewById(R.id.login_button);

        login_button.setReadPermissions(Arrays.asList("public_profile","email","user_birthday","user_friends"));

        login_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                SharedPreferencesHelper.putSharedPreferencesString(getApplicationContext(),"UTYPE_FLAG", "facebook");

                String accesstoken = loginResult.getAccessToken().getToken();

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        getData(object);

                    }
                });
                Bundle b = new Bundle();
                b.putString("fields", "id,email,first_name,last_name,picture.type(large)");
                request.setParameters(b);
                request.executeAsync();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });


    }


    @OnClick(R.id.btnLoginWithAK)
    public void loginwithAK(){
        onStartLoginPage();
    }



    public void onStartLoginPage(){
        Intent i = new Intent(this, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder = new AccountKitConfiguration.AccountKitConfigurationBuilder(LoginType.PHONE,
                AccountKitActivity.ResponseType.TOKEN);

        i.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,configurationBuilder.build());
        startActivityForResult(i,REQUEST_CODE);
        SharedPreferencesHelper.putSharedPreferencesString(getApplicationContext(),"UTYPE_FLAG", "facebook");

    }






    private void getData(JSONObject object) {
        try{
//            etUsername.setText(object.getString("first_name"+object.getString("id")));
//            etPassword.setText(object.getString(object.getString("id")));
            loginAPiWithFBCall(
                    object.getString("first_name")+" "+object.getString("id")+ " android",
                    object.getString("id"),
                    object.getString("first_name")+" "+object.getString("id")+ " android",
                    object.getString("id")+"@techband.me",
                    object.getString("id"),
                    "male",
                    "Jordan",
                    object.getString("first_name") + object.getString("last_name")
                    );
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }




    @OnClick(R.id.tv_register)
    public void register(){
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }


    @OnClick(R.id.btn_login)
    public void checkLogin(){
//        validator.validate();
        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();
        SharedPreferencesHelper.putSharedPreferencesString(getApplicationContext(),"UTYPE_FLAG", "email");

        loginAPiCall(username,password);

    }


    @Override
    public void onValidationSucceeded() {
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }


    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "للخروج اضغط مرة اخرى", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }


    private void loginAPiWithFBCall(String username, String password, final String... user) {


        new BusinessManager().loginApiCall(context, username, password, new ApiCallResponse() {
            @Override
            public void onSuccess(Object responseObject, String responseMessage) {


                if (responseMessage.equals("Unauthorized : Wrong username or password.")){
                    new BusinessManager().registerApiCall(context, user[0], user[1], user[2], user[3], user[4], user[5], new ApiCallResponse() {
                        @Override
                        public void onSuccess(Object responseObject, String responseMessage) {

                            Gson gson = new Gson();
                            String json = responseObject.toString();
                            UserModel parseObject = gson.fromJson(json, UserModel.class);
                            parseObject.setPassword(password);
//                            UserModel loginModel = (UserModel) responseObject;
                            if (parseObject != null) {
                                Utils.dismissProccessDialog();
                                loginAPiCall(username,password);
                            }else {
//                                Utils.dismissProccessDialog();
                                Toast.makeText(context, "Invalid Error ", Toast.LENGTH_LONG).show();
                            }

                        }

                        @Override
                        public void onFailure(String errorResponse) {
//                            Utils.dismissProccessDialog();
                        }
                    });
                } else {
                    UserModel loginModel = (UserModel) responseObject;
                    loginModel.setPassword(password);
                    try {
                        if (loginModel != null) {
//                            Utils.dismissProccessDialog();
                            SharedPreferencesHelper.putSharedPreferencesObject(context, SharedPrefConstants.loginObject, loginModel);
                            context.startActivity(new Intent(context, MainActivity.class));
                            finish();
                        } else {
                            Utils.dismissProccessDialog();
                            Toast.makeText(context, "error ", Toast.LENGTH_LONG).show();
                        }
                    }catch (JsonSyntaxException e) {
                        Utils.dismissProccessDialog();
                        Utils.failureDialog(context, "error");
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(String errorResponse) {
                Toast.makeText(context, "Username or Password Incorrect ", Toast.LENGTH_LONG).show();
            }
        });
    }


    private void loginAPiCall(String username, String password) {

        Utils.showProccessDialog(this);
        new BusinessManager().loginApiCall(context, username, password, new ApiCallResponse() {
            @Override
            public void onSuccess(Object responseObject, String responseMessage) {

                if (responseMessage.equals("Unauthorized : Wrong username or password.")) {
                    Utils.dismissProccessDialog();
                    Utils.failureDialog(context, "اسم المستخدم او كلمة المرور غير صحيح");
                } else if(username.equals("") || password.equals("")) {
                    Utils.failureDialog(context, "يرجى تعبئة جميع الحقول");
                } else {
                    UserModel loginModel = (UserModel) responseObject;
                    loginModel.setPassword(password);
                    try {
                        if (loginModel != null) {

                            Utils.dismissProccessDialog();
                            SharedPreferencesHelper.putSharedPreferencesObject(context, SharedPrefConstants.loginObject, loginModel);
                            refreshToken();
                            context.startActivity(new Intent(context, MainActivity.class));
                            finish();
                        } else {
                            Utils.dismissProccessDialog();
                            Toast.makeText(context, "error", Toast.LENGTH_LONG).show();
                        }
                    }catch (JsonSyntaxException e) {
                        Utils.dismissProccessDialog();
                        Utils.failureDialog(context, "اسم المستخدم او كلمة المرور غير صحيح");
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(String errorResponse) {
                Toast.makeText(context, "Username or Password Incorrect ", Toast.LENGTH_LONG).show();
            }
        });
    }



    public void refreshToken(){
        UserModel userLoginModel = (UserModel) SharedPreferencesHelper.getSharedPreferencesObject(this, SharedPrefConstants.loginObject,UserModel.class);
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.e("Dfgdfbvcfdgfd", token);
        new BusinessManager().refreshToken(this, userLoginModel.getUid(), userLoginModel.getPassword(), token, "ANDROID", new ApiCallResponse() {
            @Override
            public void onSuccess(Object responseObject, String responseMessage) {

            }

            @Override
            public void onFailure(String errorResponse) {

            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE){
            AccountKitLoginResult result = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            if(result.getError() != null){
                Toast.makeText(context, result.getError().getErrorType().getMessage(), Toast.LENGTH_SHORT).show();
                return;
            } else if(result.wasCancelled()){
                Toast.makeText(context, "cancel", Toast.LENGTH_SHORT).show();
                return;
            } else {
                AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
                    @Override
                    public void onSuccess(Account account) {

                        String username = String.valueOf(account.getPhoneNumber());
                        String usernameWithoutPlus=username.replace("+","");//replaces all occurrences of 'a' to 'e'
                        loginAPiWithFBCall(
                                usernameWithoutPlus,
                                usernameWithoutPlus,
                                usernameWithoutPlus,
                                usernameWithoutPlus+"@techband.me",
                                usernameWithoutPlus,
                                "الجنس",
                                "الدولة",
                                "الاسم الكامل"
                        );
                    }

                    @Override
                    public void onError(AccountKitError accountKitError) {

                    }
                });
            }
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }
}
