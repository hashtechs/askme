package me.techband.questionapp.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import me.techband.questionapp.R;
import me.techband.questionapp.helpers.SharedPrefConstants;
import me.techband.questionapp.helpers.SharedPreferencesHelper;
import me.techband.questionapp.interfaces.ApiCallResponse;
import me.techband.questionapp.managers.BusinessManager;
import me.techband.questionapp.models.UserModel;
import me.techband.questionapp.ui.activities.MainActivity;
import me.techband.questionapp.utils.ButtonCustomFont;
import me.techband.questionapp.utils.CheckboxCustomFont;
import me.techband.questionapp.utils.ColorsHelper;
import me.techband.questionapp.utils.EditTextCustomFont;
import me.techband.questionapp.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CreateQuestionFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;


    @BindView(R.id.et_question_content)
    EditTextCustomFont etQuestionContent;


    @BindView(R.id.et_optional_name)
    EditTextCustomFont etOptionalName;

    @BindView(R.id.ck_can_comment)
    CheckboxCustomFont ckCanComment;


    @BindView(R.id.ck_can_view_all)
    CheckboxCustomFont ckCanViewAll;

    @BindView(R.id.btn_add_question)
    ButtonCustomFont btnAddQuestion;





    public CreateQuestionFragment() {
        // Required empty public constructor
    }


    public static CreateQuestionFragment newInstance(String param1, String param2) {
        CreateQuestionFragment fragment = new CreateQuestionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_question, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @OnClick(R.id.btn_add_question)
    public void addQuestion(){

        UserModel userLoginModel = (UserModel) SharedPreferencesHelper.getSharedPreferencesObject(getContext(), SharedPrefConstants.loginObject,UserModel.class);

        Utils.showProccessDialog(getActivity());
        String fieldQuestion = etQuestionContent.getText().toString();
        String fieldQuestionerName = etOptionalName.getText().toString();
        String fieldQuestionerCountry = userLoginModel.getNationality();
        String fieldQuestionerCity = "";
        String fieldQuestionerAge = "";
        String fieldQuestionerSex = userLoginModel.getUser_gender();
        String fieldQuestionAnswer = "";
        String fieldQuestionType = "";
        int fieldUid = Integer.parseInt(userLoginModel.getUid());

        int canComment = 0;
        if (ckCanComment.isChecked()){
            canComment = 1;
        }

        int canPublic = 0;
        if (ckCanViewAll.isChecked()){
            canPublic = 1;
        }

        int fieldCommentsAllowed = canComment;
        int fieldIsPublic = canPublic;


        if (fieldQuestion.equals("")){
            Utils.dismissProccessDialog();
            Utils.failureDialog(getContext(),"يرجى تعبئة جميع الحقول");
        } else if(!Utils.haveNetworkConnection(getActivity())) {
            Utils.dialogErrorInternet(getActivity());
            Utils.dismissProccessDialog();
        }else{
            new BusinessManager().addQuestions(getContext(), new ApiCallResponse() {
                        @Override
                        public void onSuccess(Object responseObject, String responseMessage) {
                            Utils.dismissProccessDialog();
                            Utils.successDialog(getContext(),"تمت إضافة سؤالك بنجاح");
                            resetForm();
                        }

                        @Override
                        public void onFailure(String errorResponse) {

                        }
                    },fieldQuestion,
                    fieldQuestionerName.length() == 0 ? "مجهول" : fieldQuestionerName,
                    fieldQuestionerCountry == null ? "jordan" : userLoginModel.getNationality(),
                    fieldQuestionerCity,
                    fieldQuestionerAge,
                    fieldQuestionerSex == null ? "male" : userLoginModel.getUser_gender(),
                    fieldQuestionAnswer,fieldQuestionType,fieldUid,fieldCommentsAllowed,fieldIsPublic);
        }

    }


    public void resetForm(){
        etOptionalName.setText("");
        etQuestionContent.setText("");
        ckCanViewAll.setChecked(false);
        ckCanComment.setChecked(false);
    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
