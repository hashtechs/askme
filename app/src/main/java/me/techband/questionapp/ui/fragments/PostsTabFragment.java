package me.techband.questionapp.ui.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import me.techband.questionapp.adapters.PostsAdapter;
import me.techband.questionapp.R;
import me.techband.questionapp.interfaces.ApiCallResponseCustom;
import me.techband.questionapp.managers.BusinessManager;
import me.techband.questionapp.models.QuestionModel;
import me.techband.questionapp.utils.EndlessRecyclerViewScrollListener;
import me.techband.questionapp.utils.PaginationScrollListener;
import me.techband.questionapp.utils.Utils;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import retrofit2.http.PUT;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PostsTabFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PostsTabFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PostsTabFragment extends Fragment{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;


    LinearLayoutManager manager;

    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    // limiting to 5 for this tutorial, since total pages in actual API is very large. Feel free to modify.
    private int TOTAL_PAGES = 9;
    private int currentPage = PAGE_START;



    @BindView(R.id.rvPosts)
    RecyclerView rvPosts;


    QuestionModel[] questionModel;
    PostsAdapter adapter;


    public PostsTabFragment() {
        // Required empty public constructor
    }


    public static PostsTabFragment newInstance(String param1, String param2) {
        PostsTabFragment fragment = new PostsTabFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_posts_tab, container, false);
        rvPosts = view.findViewById(R.id.rvPosts);


        adapter = new PostsAdapter(getContext());

        manager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvPosts.setLayoutManager(manager);

        rvPosts.setItemAnimator(new DefaultItemAnimator());

        rvPosts.setAdapter(adapter);

        rvPosts.addOnScrollListener(new PaginationScrollListener(manager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getAllPosts(currentPage,false);
                    }
                }, 1000);
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        getAllPosts(1,true);

        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    public void getAllPosts(int page,boolean isFirst){

        if (isFirst)
            Utils.showProccessDialog(getActivity());

        new BusinessManager().getAllPosts(page,new ApiCallResponseCustom() {
            @Override
            public void onSuccess(Object[] responseObject, String responseMessage) {

                if (isFirst){

                    if (Utils.isProgressShowing()){
                        Utils.dismissProccessDialog();
                    }

                    questionModel = (QuestionModel[]) responseObject;

                    List<QuestionModel> results = Arrays.asList(questionModel);
                    adapter.addAll(results);

                    if (currentPage <= TOTAL_PAGES) adapter.addLoadingFooter();
                    else isLastPage = true;

                } else {
                    if (Utils.isProgressShowing()){
                        Utils.dismissProccessDialog();
                    }


                    questionModel = (QuestionModel[]) responseObject;

                    adapter.removeLoadingFooter();
                    isLoading = false;

                    List<QuestionModel> results = Arrays.asList(questionModel);
                    adapter.addAll(results);

                    if (currentPage != TOTAL_PAGES) adapter.addLoadingFooter();
                    else isLastPage = true;

                }


            }

            @Override
            public void onFailure(String errorResponse) {
                if (Utils.isProgressShowing()){
                    Utils.dismissProccessDialog();
                }
            }
        });
    }


    public void getAllPosts2(int page){
        new BusinessManager().getAllPosts(page,new ApiCallResponseCustom() {
            @Override
            public void onSuccess(Object[] responseObject, String responseMessage) {



            }

            @Override
            public void onFailure(String errorResponse) {
                if (Utils.isProgressShowing()){
                    Utils.dismissProccessDialog();
                }
            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (Utils.isProgressShowing()){
            Utils.dismissProccessDialog();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Utils.isProgressShowing()){
            Utils.dismissProccessDialog();
        }
    }



    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
