package me.techband.questionapp.ui.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import me.techband.questionapp.R;
import me.techband.questionapp.adapters.CommentAdapter;
import me.techband.questionapp.helpers.SharedPrefConstants;
import me.techband.questionapp.helpers.SharedPreferencesHelper;
import me.techband.questionapp.interfaces.ApiCallResponse;
import me.techband.questionapp.interfaces.ApiCallResponseCustom;
import me.techband.questionapp.managers.BusinessManager;
import me.techband.questionapp.models.CommentModel;
import me.techband.questionapp.models.UserModel;
import me.techband.questionapp.ui.activities.MainActivity;
import me.techband.questionapp.utils.ButtonCustomFont;
import me.techband.questionapp.utils.EditTextCustomFont;
import me.techband.questionapp.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class CreateCommentFragment extends Fragment {

    View rootView = null;
    Context context;

    public static String questionIdd;
    CommentAdapter commentAdapter;
    CommentModel[] commentList;



    @BindView(R.id.et_optional_name)
    EditTextCustomFont etOptionalName;


    @BindView(R.id.et_question_content)
    EditTextCustomFont etCommentContent;

    @BindView(R.id.btn_add_comment)
    ButtonCustomFont btnAddComment;





    public CreateCommentFragment() {
        // Required empty public constructor
    }

    public static CreateCommentFragment newInstance(String questionId) {
        questionIdd = questionId;
        return new CreateCommentFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null)
                parent.removeView(rootView);
        }
        View rootView = inflater.inflate(R.layout.fragment_create_comment, container, false);
        context = getActivity();
        ((MainActivity) context).imgComments.setVisibility(View.GONE);
        ((MainActivity) context).mToolBarTextView.setText("إضافة تعليق");
        ButterKnife.bind(this, rootView);


        context = getActivity();
        ButterKnife.bind(this, rootView);


        return rootView;
    }


    @OnClick(R.id.btn_add_comment)
    public void addComment(){

        UserModel userLoginModel = (UserModel) SharedPreferencesHelper.getSharedPreferencesObject(getContext(), SharedPrefConstants.loginObject,UserModel.class);

        Utils.showProccessDialog(getActivity());
        String commentBody = etCommentContent.getText().toString();
        String optionalName = etOptionalName.getText().toString();
        String uid = userLoginModel.getUid();
        String nid = questionIdd;

        if (commentBody.equals("")){
            Utils.dismissProccessDialog();
            Utils.failureDialog(getContext(),"يرجى تعبئة جميع الحقول");
        } else {
            new BusinessManager().addComment(getContext(), new ApiCallResponse() {
                        @Override
                        public void onSuccess(Object responseObject, String responseMessage) {
                            Utils.dismissProccessDialog();
                            getActivity().onBackPressed();
                        }

                        @Override
                        public void onFailure(String errorResponse) {

                        }
                    },nid,
                    optionalName.length() == 0 ? "مجهول" : optionalName,
                    uid,
                    commentBody);
        }

    }



//    public void getCommentsById(){
//        new BusinessManager().getCommentsById(new ApiCallResponseCustom() {
//            @Override
//            public void onSuccess(Object[] responseObject, String responseMessage) {
//                commentList = (CommentModel[]) responseObject;
//                if (commentList.length == 0){
//                    noCommentGroup.setVisibility(View.VISIBLE);
//                    rvComment.setVisibility(View.GONE);
//                    btnNewComment.setVisibility(View.GONE);
//                } else {
//                    btnNewComment.setVisibility(View.VISIBLE);
//                }
//                rvComment.setLayoutManager(new LinearLayoutManager(context));
//                commentAdapter = new CommentAdapter(getActivity(), commentList);
//                rvComment.setAdapter(commentAdapter);
//                commentAdapter.notifyDataSetChanged();
//            }
//
//            @Override
//            public void onFailure(String errorResponse) {
//
//            }
//        },questionIdd);
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }




    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.menu_comments, menu);
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item=menu.findItem(R.id.context_menu);
        item.setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.back:
                getActivity().onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
