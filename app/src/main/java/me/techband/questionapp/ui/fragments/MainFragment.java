package me.techband.questionapp.ui.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import me.techband.questionapp.R;
import me.techband.questionapp.utils.RadioButtonCustomFont;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;

public class MainFragment extends Fragment implements FragmentManager.OnBackStackChangedListener{

    @BindView(R.id.rb_posts_with_comments)
    RadioButtonCustomFont rbPostWithCommentsTab;
    @BindView(R.id.rb_posts_tab)
    RadioButtonCustomFont rbPostsTab;


    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        getChildFragmentManager().addOnBackStackChangedListener(this);

        PostsTabFragment fragment = new PostsTabFragment();
        getChildFragmentManager().beginTransaction()
                .add(R.id.container_fragment_offers, fragment, "POSTS")
                .commit();

        changeState(rbPostsTab, true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_posts, container, false);
    }

    @OnCheckedChanged(R.id.rb_posts_tab)
    void onTodayChecked(RadioButton button, boolean checked) {
        if (checked) {
            getChildFragmentManager().popBackStack();
        }
        changeState(button, checked);
    }

    @OnCheckedChanged(R.id.rb_posts_with_comments)
    void onBestChecked(RadioButton button, boolean checked) {
        if (checked) {
            PostsWithCommentTabFragment fragment = new PostsWithCommentTabFragment();
            getChildFragmentManager().beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .addToBackStack("POSTSCOMMENTTAB")
                    .replace(R.id.container_fragment_offers, fragment, "POSTSCOMMENTTAB")
                    .commit();
        }
        changeState(button, checked);
    }


    private void changeState(RadioButton button, boolean isChecked) {

        if (rbPostWithCommentsTab.isChecked()){
            rbPostWithCommentsTab.setTextColor(Color.parseColor("#FFFFFF"));
            rbPostsTab.setTextColor(Color.parseColor("#1e1e3b"));
        } else {
            rbPostsTab.setTextColor(Color.parseColor("#FFFFFF"));
            rbPostWithCommentsTab.setTextColor(Color.parseColor("#1e1e3b"));
        }
    }

    @Override
    public void onBackStackChanged() {
        int count = getChildFragmentManager().getBackStackEntryCount();
        String name;
        if (count == 0) {
            name = "POSTSTAB";
        } else {
            name = getChildFragmentManager().getBackStackEntryAt(count - 1).getName();
        }
        if (name == null) {
            return;
        }
        switch (name) {
            case "POSTSTAB":
                rbPostsTab.setChecked(true);
                break;
            case "POSTSCOMMENT":
                rbPostsTab.setChecked(true);
                break;
        }
    }
}
