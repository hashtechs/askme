package me.techband.questionapp.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import me.techband.questionapp.R;
import me.techband.questionapp.adapters.CommentAdapter;
import me.techband.questionapp.interfaces.ApiCallResponseCustom;
import me.techband.questionapp.managers.BusinessManager;
import me.techband.questionapp.models.CommentModel;
import me.techband.questionapp.ui.activities.MainActivity;
import me.techband.questionapp.utils.ButtonCustomFont;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class CommentsFragment extends Fragment {

    View rootView = null;
    Context context;

    public static String questionIdd;
    CommentAdapter commentAdapter;
    CommentModel[] commentList;

    @BindView(R.id.rv_comment)
    RecyclerView rvComment;

    @BindView(R.id.no_comment_group)
    LinearLayout noCommentGroup;


    @BindView(R.id.btn_new_comment)
    ButtonCustomFont btnNewComment;





    public CommentsFragment() {
        // Required empty public constructor
    }

    public static CommentsFragment newInstance(String questionId) {
        questionIdd = questionId;
        return new CommentsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null)
                parent.removeView(rootView);
        }
        View rootView = inflater.inflate(R.layout.fragment_comments, container, false);
        context = getActivity();
        ((MainActivity) context).imgComments.setVisibility(View.GONE);
        ((MainActivity) context).mToolBarTextView.setText("التعليقات");
        ButterKnife.bind(this, rootView);


        context = getActivity();
        ButterKnife.bind(this, rootView);

        getCommentsById();

        return rootView;
    }



    public void getCommentsById(){
        new BusinessManager().getCommentsById(getContext(),new ApiCallResponseCustom() {
            @Override
            public void onSuccess(Object[] responseObject, String responseMessage) {
                commentList = (CommentModel[]) responseObject;
                if (commentList.length == 0){
                    noCommentGroup.setVisibility(View.VISIBLE);
                    rvComment.setVisibility(View.GONE);
                    btnNewComment.setVisibility(View.GONE);
                } else {
                    btnNewComment.setVisibility(View.VISIBLE);
                }
                rvComment.setLayoutManager(new LinearLayoutManager(context));
                commentAdapter = new CommentAdapter(getActivity(), commentList);
                rvComment.setAdapter(commentAdapter);
                commentAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(String errorResponse) {

            }
        },questionIdd);
    }


    @OnClick({R.id.btn_new_comment, R.id.btn_add_comment})
    public void moveTOMakeComment(){
        ((MainActivity) context).changeFragmentMethod(CreateCommentFragment.newInstance(questionIdd), "see All");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }




    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.menu_comments, menu);
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item=menu.findItem(R.id.context_menu);
        item.setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.back:
                getActivity().onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
