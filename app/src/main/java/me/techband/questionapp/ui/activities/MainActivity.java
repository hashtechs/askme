package me.techband.questionapp.ui.activities;

import android.app.NotificationManager;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.iid.FirebaseInstanceId;

import me.techband.questionapp.helpers.AnalyticsApplication;
import me.techband.questionapp.helpers.SharedPrefConstants;
import me.techband.questionapp.helpers.SharedPreferencesHelper;
import me.techband.questionapp.ui.fragments.ContactFragment;
import me.techband.questionapp.ui.fragments.CreateQuestionFragment;
import me.techband.questionapp.ui.fragments.MainFragment;
import me.techband.questionapp.ui.fragments.MyQuestionsFragment;
import me.techband.questionapp.ui.fragments.PostsFragment;
import me.techband.questionapp.R;
import me.techband.questionapp.ui.fragments.ProfileFragment;
import me.techband.questionapp.utils.TextviewCustomFont;
import me.techband.questionapp.contextmenu.ContextMenuDialogFragment;
import me.techband.questionapp.contextmenu.MenuObject;
import me.techband.questionapp.contextmenu.MenuParams;
import me.techband.questionapp.contextmenu.interfaces.OnMenuItemClickListener;
import me.techband.questionapp.contextmenu.interfaces.OnMenuItemLongClickListener;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity implements OnMenuItemClickListener, OnMenuItemLongClickListener {

    private FragmentManager fragmentManager;
    private ContextMenuDialogFragment mMenuDialogFragment;

    @BindView(R.id.imgComments)
    public ImageView imgComments;
    public TextviewCustomFont mToolBarTextView;
    boolean doubleBackToExitPressedOnce = false;
    private AdView mAdView;
    Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        fragmentManager = getSupportFragmentManager();
        initToolbar();
        initMenuFragment();
        addFragment(new MyQuestionsFragment(), true, R.id.container);
        MobileAds.initialize(this, "ca-app-pub-4769590216291024~5982205877");
//        MobileAds.initialize(this, "ca-app-pub-3940256099942544~3347511713");


        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("4DD0986B8BB49093161F4F00CF61B887")// Add your real device id here
                .build();

        mAdView.loadAd(adRequest);

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest messageDigest = MessageDigest.getInstance("SHA");
                messageDigest.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(messageDigest.digest(), Base64.DEFAULT));
            }
        }
        catch (PackageManager.NameNotFoundException e) {

        }
        catch (NoSuchAlgorithmException e) {

        }









//        Fabric.with(this, new Crashlytics());
//
//
//        Button crashButton = new Button(this);
//        crashButton.setText("Crash!");
//        crashButton.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View view) {
//                Crashlytics.getInstance().crash(); // Force a crash
//            }
//        });
//
//        final Fabric fabric = new Fabric.Builder(this)
//                .kits(new Crashlytics())
//                .debuggable(true)  // Enables Crashlytics debugger
//                .build();
//        Fabric.with(fabric);
//
//        addContentView(crashButton, new ViewGroup.LayoutParams(
//                ViewGroup.LayoutParams.MATCH_PARENT,
//                ViewGroup.LayoutParams.WRAP_CONTENT));

//              Configuration configuration = getResources().getConfiguration();
//        configuration.setLayoutDirection(new Locale("ar"));
//        getResources().updateConfiguration(configuration, getResources().getDisplayMetrics());

    }




    private void initMenuFragment() {
        MenuParams menuParams = new MenuParams();
        menuParams.setActionBarSize((int) getResources().getDimension(R.dimen.tool_bar_height));
        menuParams.setMenuObjects(getMenuObjects());
        menuParams.setClosableOutside(false);
        menuParams.setAnimationDuration(35);
        mMenuDialogFragment = ContextMenuDialogFragment.newInstance(menuParams);
        mMenuDialogFragment.setItemClickListener(this);
        mMenuDialogFragment.setItemLongClickListener(this);
    }




    private List<MenuObject> getMenuObjects() {
        // You can use any [resource, bitmap, drawable, color] as image:
        // item.setResource(...)
        // item.setBitmap(...)
        // item.setDrawable(...)
        // item.setColor(...)
        // You can set image ScaleType:
        // item.setScaleType(ScaleType.FIT_XY)
        // You can use any [resource, drawable, color] as background:
        // item.setBgResource(...)
        // item.setBgDrawable(...)
        // item.setBgColor(...)
        // You can use any [color] as text color:
        // item.setTextColor(...)
        // You can set any [color] as divider color:
        // item.setDividerColor(...)

        List<MenuObject> menuObjects = new ArrayList<>();


        MenuObject close = new MenuObject();
        close.setResource(R.drawable.ic_close_black_24dp);
        close.setBgColor(getResources().getColor(R.color.MidnightBlue));

        MenuObject myquestions = new MenuObject("أسئلتي");
        myquestions.setResource(R.drawable.ic_my_question);
        myquestions.setBgColor(getResources().getColor(R.color.MidnightBlue));

        MenuObject allquestions = new MenuObject("المشاركات");
        allquestions.setResource(R.drawable.ic_all_question);
        allquestions.setBgColor(getResources().getColor(R.color.MidnightBlue));

        MenuObject addquestion = new MenuObject("إسال");
        addquestion.setResource(R.drawable.ic_add_question);
        addquestion.setBgColor(getResources().getColor(R.color.MidnightBlue));

        MenuObject profile = new MenuObject("الملف الشخصي");
        profile.setResource(R.drawable.ic_man_user);
        profile.setBgColor(getResources().getColor(R.color.MidnightBlue));

        MenuObject contactus = new MenuObject("تواصل معنا");
        contactus.setResource(R.drawable.ic_add);
        contactus.setBgColor(getResources().getColor(R.color.MidnightBlue));

        MenuObject share = new MenuObject("مشاركة التطبيق");
        share.setResource(R.drawable.ic_share);
        share.setBgColor(getResources().getColor(R.color.MidnightBlue));

        MenuObject logout = new MenuObject("خروج");
        logout.setResource(R.drawable.ic_logout);
        logout.setBgColor(getResources().getColor(R.color.MidnightBlue));


        menuObjects.add(close);
        menuObjects.add(myquestions);
        menuObjects.add(allquestions);
        menuObjects.add(addquestion);
        menuObjects.add(profile);
        menuObjects.add(contactus);
        menuObjects.add(share);
        menuObjects.add(logout);
        return menuObjects;


    }

    private void initToolbar() {
        Toolbar mToolbar = findViewById(R.id.toolbar);
        mToolBarTextView = findViewById(R.id.text_view_toolbar_title);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
//            getSupportActionBar().setHomeButtonEnabled(true);
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
//        mToolbar.setNavigationIcon(R.drawable.btn_back);
//        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onBackPressed();
//            }
//        });
        mToolBarTextView.setText("اسالني");
    }

    protected void addFragment(Fragment fragment, boolean addToBackStack, int containerId) {
        invalidateOptionsMenu();
        String backStackName = fragment.getClass().getName();
        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStackName, 0);
        if (!fragmentPopped) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.add(containerId, fragment, backStackName)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            if (addToBackStack)
                transaction.addToBackStack(backStackName);
            transaction.commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.context_menu:
                if (fragmentManager.findFragmentByTag(ContextMenuDialogFragment.TAG) == null) {
                    mMenuDialogFragment.show(fragmentManager, ContextMenuDialogFragment.TAG);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (fragmentManager.getBackStackEntryCount() == 1) {
        } else {
            super.onBackPressed();

        }
    }

    @Override
    public void onMenuItemClick(View clickedView, int position) {

        switch (position){
            case 1:
                changeFragmentMethod(new MyQuestionsFragment(), "أسئلتي");
                break;
            case 2:
                changeFragmentMethod(new PostsFragment(), "المشاركات");
                break;
            case 3:
                changeFragmentMethod(new CreateQuestionFragment(), "اسال سؤال");
                break;
            case 4:
                changeFragmentMethod(new ProfileFragment(), "الملف الشخصي");
                break;
            case 5:
                changeFragmentMethod(new ContactFragment(), "تواصل معنا");
                break;
            case 6:
                try {
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, "إسألني");
                    String title = "\nتحميل تطبيق إسألني\n\n";
                    title = title + "https://play.google.com/store/apps/details?id=me.techband.questionapp \n\n";
                    i.putExtra(Intent.EXTRA_TEXT, title);
                    startActivity(Intent.createChooser(i, "choose one"));
                } catch(Exception e) {
                    //e.toString();
                }
                break;
            case 7:
                SharedPreferencesHelper.clear(this);
                startActivity(new Intent(this, LoginActivity.class));


                break;
        }
    }

    @Override
    public void onMenuItemLongClick(View clickedView, int position) {
        Toast.makeText(this, "Long clicked on position: " + position, Toast.LENGTH_SHORT).show();
    }


    public void changeFragmentMethod(Fragment targetFragment, String title) {
        if (targetFragment != null) {
            fragmentManager = getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.setCustomAnimations(R.anim.slide_in, R.anim.slide_out);
            ft.replace(R.id.container, targetFragment);
            ft.addToBackStack("backStack");
            ft.commit();
            mToolBarTextView.setText(title);

        }
    }


    public void changeFragmentTabsMethod(Fragment targetFragment, String title, int type) {
        if (targetFragment != null) {
            fragmentManager = getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.setCustomAnimations(R.anim.slide_in, R.anim.slide_out);
            ft.replace(R.id.container_fragment_offers, targetFragment, title);
            if(type == 1){
                ft.addToBackStack(title);
            }
            ft.commit();
        }
    }







}
