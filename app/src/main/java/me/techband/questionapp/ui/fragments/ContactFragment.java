package me.techband.questionapp.ui.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import me.techband.questionapp.R;
import me.techband.questionapp.helpers.SharedPrefConstants;
import me.techband.questionapp.helpers.SharedPreferencesHelper;
import me.techband.questionapp.interfaces.ApiCallResponse;
import me.techband.questionapp.managers.BusinessManager;
import me.techband.questionapp.models.UserModel;
import me.techband.questionapp.utils.ButtonCustomFont;
import me.techband.questionapp.utils.EditTextCustomFont;
import me.techband.questionapp.utils.TextviewCustomFont;
import me.techband.questionapp.utils.Utils;


public class ContactFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;


    EditTextCustomFont etName, etSubject, etMobile, etEmail, etMsg;
    ButtonCustomFont btnAddContact;

    public ContactFragment() {
        // Required empty public constructor
    }


    public static ContactFragment newInstance(String param1, String param2) {
        ContactFragment fragment = new ContactFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_contact, container, false);
        // Inflate the layout for this fragment

        initViews(view);


        btnAddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addContact();
            }
        });
        return view;
    }


    public void initViews(View view){
        etName = (EditTextCustomFont) view.findViewById(R.id.et_name);
        etSubject = (EditTextCustomFont) view.findViewById(R.id.et_subject);
        etMobile = (EditTextCustomFont) view.findViewById(R.id.et_mobile);
        etEmail = (EditTextCustomFont) view.findViewById(R.id.et_email);
        etMsg = (EditTextCustomFont) view.findViewById(R.id.et_msg);
        btnAddContact = (ButtonCustomFont) view.findViewById(R.id.btn_add_contact);
    }


    public void addContact(){

        UserModel userLoginModel = (UserModel) SharedPreferencesHelper.getSharedPreferencesObject(getContext(), SharedPrefConstants.loginObject,UserModel.class);

        Utils.showProccessDialog(getActivity());
        String msg = etMsg.getText().toString();
        String name = etName.getText().toString();
        String email = etEmail.getText().toString();
        String mobile = etEmail.getText().toString();
        String subject = etSubject.getText().toString();


        if (Utils.isEmailValid(email) || msg.equals("") || name.equals("") || email.equals("") || mobile.equals("")){
            Utils.dismissProccessDialog();
            Utils.failureDialog(getContext(),"يرجى تعبئة جميع الحقول");
        } else {
            new BusinessManager().addContact(getContext(), new ApiCallResponse() {
                        @Override
                        public void onSuccess(Object responseObject, String responseMessage) {
                            Utils.dismissProccessDialog();
                            Utils.successDialog(getContext(), "تمت اضافة ملاححظتك بنجاح");
                            resetForm();
                        }

                        @Override
                        public void onFailure(String errorResponse) {

                        }
                    },msg,
                    name.length() == 0 ? userLoginModel.getFullname() : name,
                    email.length() == 0 ? userLoginModel.getMail() : email,
                    mobile,
                    "android",
                    msg,
                    subject);
        }

    }


    public void resetForm(){
        etName.setText("");
        etSubject.setText("");
        etEmail.setText("");
        etMsg.setText("");
        etMobile.setText("");
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
