package me.techband.questionapp.ui.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import me.techband.questionapp.R;

import me.techband.questionapp.adapters.MyItemAnimator;
import me.techband.questionapp.adapters.QuestionAdapter;
import me.techband.questionapp.helpers.SharedPrefConstants;
import me.techband.questionapp.helpers.SharedPreferencesHelper;
import me.techband.questionapp.interfaces.ApiCallResponseCustom;
import me.techband.questionapp.interfaces.ApiCallResponseCustomQuestions;
import me.techband.questionapp.managers.BusinessManager;
import me.techband.questionapp.models.QuestionModel;
import me.techband.questionapp.models.UserModel;
import me.techband.questionapp.utils.TextviewCustomFont;
import me.techband.questionapp.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class AnswerdTabFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    RecyclerView rvAnswerdQuestion;

    private QuestionAdapter answeredQuestionAdapter;

    QuestionModel[] questionModel;

    QuestionModel[] filteredQuestion;

    private int index = 0;


    TextviewCustomFont tvNoQuestions;


    public AnswerdTabFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static AnswerdTabFragment newInstance(String param1, String param2) {
        AnswerdTabFragment fragment = new AnswerdTabFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_answerd_tab, container, false);

        rvAnswerdQuestion = view.findViewById(R.id.rvAnswerdQuestions);
        tvNoQuestions = view.findViewById(R.id.tv_no_questions);

        getMyQuestions();

        return view;
    }


    public void getMyQuestions() {
        Utils.showProccessDialog(getActivity());
        final UserModel userLoginModel = (UserModel) SharedPreferencesHelper.getSharedPreferencesObject(getContext(), SharedPrefConstants.loginObject, UserModel.class);
        new BusinessManager().getMyQuestions(getActivity(), Integer.parseInt(userLoginModel.getUid()), new ApiCallResponseCustom() {
            @Override
            public void onSuccess(Object[] responseObject, String responseMessage) {
                questionModel = (QuestionModel[]) responseObject;


                List<QuestionModel> listFilter = new ArrayList<>();
                for (QuestionModel question : questionModel) {
                    if (question.getQuestionAnswer() != null && !question.getQuestionAnswer().toString().isEmpty()) {
                        listFilter.add(question);
                    }
                }

                if (listFilter.size() == 0) {
                    tvNoQuestions.setVisibility(View.VISIBLE);
                    rvAnswerdQuestion.setVisibility(View.GONE);
                    Utils.dismissProccessDialog();
                } else {

                    rvAnswerdQuestion.setLayoutManager(new LinearLayoutManager(getActivity()));
                    answeredQuestionAdapter = new QuestionAdapter(getActivity(), listFilter, true);
                    rvAnswerdQuestion.setAdapter(answeredQuestionAdapter);

                    rvAnswerdQuestion.setItemAnimator(new MyItemAnimator());
                    answeredQuestionAdapter.notifyDataSetChanged();
                    if (Utils.isProgressShowing()) {
                        Utils.dismissProccessDialog();
                    }
                }
            }


            @Override
            public void onFailure(String errorResponse) {
                if (Utils.isProgressShowing()) {
                    Utils.dismissProccessDialog();
                }
            }
        });
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
