package me.techband.questionapp.ui.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import me.techband.questionapp.R;
import me.techband.questionapp.models.QuestionModel;
import me.techband.questionapp.ui.activities.MainActivity;
import me.techband.questionapp.utils.TextviewCustomFont;
import me.techband.questionapp.utils.TextviewCustomFontBold;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QuestionDetailsFragment extends Fragment {


    View rootView = null;
    Context context;

    public static QuestionModel questionModel;
    public static Integer bgColor;

    @BindView(R.id.tv_question_content)
    TextviewCustomFontBold tvQuestionContent;

    @BindView(R.id.tv_question_date)
    TextviewCustomFontBold tvQuestionDate;

    @BindView(R.id.tv_answer_content)
    TextviewCustomFontBold tvAnswerContent;

    @BindView(R.id.tv_answer_title)
    TextviewCustomFontBold tvAnswerTitle;


    public QuestionDetailsFragment() {
        // Required empty public constructor
    }

    public static QuestionDetailsFragment newInstance(QuestionModel questionsList, Integer color) {
        questionModel = questionsList;
        bgColor = color;
        return new QuestionDetailsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null)
                parent.removeView(rootView);
        }
        View rootView = inflater.inflate(R.layout.fragment_question_details, container, false);
        context = getActivity();
        ((MainActivity) context).imgComments.setVisibility(View.VISIBLE);
        ((MainActivity) context).imgComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) context).changeFragmentMethod(CommentsFragment.newInstance(questionModel.getQuestionId().toString()), "see All");
            }
        });
        ((MainActivity) context).mToolBarTextView.setText("السؤال "+questionModel.getQuestionId());
        rootView.setBackgroundColor(bgColor);
        ButterKnife.bind(this, rootView);
        tvQuestionDate.setText("تاريخ السؤال :"+questionModel.getQuestionDate().toString());
        tvQuestionContent.setText(questionModel.getQuestion().toString());
        if (!questionModel.getQuestionAnswer().equals("")){
            tvAnswerContent.setText(questionModel.getQuestionAnswer().toString());
        } else {
            tvAnswerTitle.setVisibility(View.GONE);
            tvAnswerContent.setVisibility(View.GONE);
        }

        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }




    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.menu_comments, menu);
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item=menu.findItem(R.id.context_menu);
        item.setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.back:
                getActivity().onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }



}
