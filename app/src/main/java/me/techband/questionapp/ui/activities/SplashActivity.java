package me.techband.questionapp.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import me.techband.questionapp.R;
import me.techband.questionapp.utils.ConnectionDetector;
import me.techband.questionapp.utils.Utils;

public class SplashActivity extends AppCompatActivity {


    private static int SPLASH_TIME_OUT = 3000;
    Context context = this;
    private ConnectionDetector detector;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                if (Utils.isLogin(context))
//                {
                if (Utils.isLogin(context))
                {
                    context.startActivity(new Intent(context, MainActivity.class));
                    finish();
                }else {
//                    context.startActivity(new Intent(context, .class));
//                    finish();
                }
//                    finish();
//                }else {
//                    context.startActivity(new Intent(context, TutorialActivity.class));
//                    finish();
//                }

            }
        }, 2300);

        detector = new ConnectionDetector(SplashActivity.this);
        if (!detector.isInternetAvailable()) {
            this.registerReceiver(this.mConnReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }

    }


    private BroadcastReceiver mConnReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            NetworkInfo currentNetworkInfo = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
            if (currentNetworkInfo.isConnected()) {
                finish();
                startActivity(getIntent());
            } else {
                Toast.makeText(context, context.getResources().getString(R.string.no_internt), Toast.LENGTH_LONG).show();
            }
        }
    };
}
