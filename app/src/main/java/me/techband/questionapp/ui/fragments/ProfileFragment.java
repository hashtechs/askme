package me.techband.questionapp.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.annotation.Select;
import me.techband.questionapp.R;
import me.techband.questionapp.helpers.SharedPrefConstants;
import me.techband.questionapp.helpers.SharedPreferencesHelper;
import me.techband.questionapp.interfaces.ApiCallResponse;
import me.techband.questionapp.managers.BusinessManager;
import me.techband.questionapp.models.UserModel;
import me.techband.questionapp.ui.activities.MainActivity;
import me.techband.questionapp.utils.ButtonCustomFont;
import me.techband.questionapp.utils.EditTextCustomFont;
import me.techband.questionapp.utils.SpinnerUtil;
import me.techband.questionapp.utils.TextviewCustomFont;
import me.techband.questionapp.utils.Utils;
import com.ybs.countrypicker.CountryPicker;
import com.ybs.countrypicker.CountryPickerListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;


    EditTextCustomFont etUsername;

    EditTextCustomFont etEmail;

    EditTextCustomFont etName;

    TextviewCustomFont etCountry;

    Spinner spGender;



    UserModel userLoginModel;

    CountryPicker picker;
    ButtonCustomFont btnSave;
    ButtonCustomFont btnChangePassword;

    public ProfileFragment() {
        // Required empty public constructor
    }


    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        ((MainActivity) getContext()).imgComments.setVisibility(View.GONE);
        initViews(view);
        userLoginModel = (UserModel) SharedPreferencesHelper.getSharedPreferencesObject(getContext(), SharedPrefConstants.loginObject,UserModel.class);
        ((MainActivity) getActivity()).mToolBarTextView.setText("الملف الشخصي");

        setProfileInfo();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });

        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getContext()).changeFragmentMethod(new ChangePasswordFragment(),"");
            }
        });


        SpinnerUtil.getInstance().setupSpinner(spGender, R.array.gender_array, getContext());

        if (userLoginModel.getUser_gender().equals("ذكر") || userLoginModel.getUser_gender().equals("male")){
            spGender.setSelection(1);
        } else if (userLoginModel.getUser_gender().equals("أنثى") || userLoginModel.getUser_gender().equals("female")){
            spGender.setSelection(2);
        }


        picker = CountryPicker.newInstance("Select Country");  // dialog title
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                etCountry.setText(name);
                picker.dismiss();

            }
        });


        etCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picker.show(getActivity().getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });
        return view;
    }


    public void initViews(View view){
        etUsername = view.findViewById(R.id.et_username);
        etName = view.findViewById(R.id.et_name);
        etEmail = view.findViewById(R.id.et_email);
        spGender = view.findViewById(R.id.sp_gender);
        etCountry = view.findViewById(R.id.sp_country);
        btnSave = view.findViewById(R.id.btn_save);
        btnChangePassword = view.findViewById(R.id.btn_change_password);

        String userType = SharedPreferencesHelper.getSharedPreferencesString(getContext(),"UTYPE_FLAG","UTYPE_FLAG");

        Log.e("dffdgg", userType);
        if (userType.equals("facebook")){
            btnChangePassword.setVisibility(View.GONE);
        } else {
            btnChangePassword.setVisibility(View.VISIBLE);
        }
    }


    public void setProfileInfo(){
        etUsername.setText(userLoginModel.getName());
        String username = etUsername.getText().toString();
        if (username.contains("android")){
            String[] parts = username.split(" ");
            String part1 = parts[0];
            String part2 = parts[1];
            etUsername.setText(part1+" "+part2);
        }else {
            etUsername.setText(userLoginModel.getName());
        }
        etEmail.setText(userLoginModel.getMail());
        etName.setText(userLoginModel.getFullname());

        etCountry.setText(userLoginModel.getNationality());
        }

    public void save(){

        final String username = etUsername.getText().toString();
        final String email = etEmail.getText().toString();
        final String name = etName.getText().toString();
        final String gender = spGender.getSelectedItem().toString();
        final String country = etCountry.getText().toString();

        Utils.showProccessDialog(getActivity());
        new BusinessManager().updateProfileCall(getContext(), userLoginModel.getUid(), username, email, gender, country, name, new ApiCallResponse() {
            @Override
            public void onSuccess(Object responseObject, String responseMessage) {
                if (Utils.isProgressShowing()){
                    Utils.dismissProccessDialog();
                }
                UserModel loginModel = new UserModel();
                loginModel.setFullname(name);
                loginModel.setUser_gender(gender);
                loginModel.setMail(email);
                loginModel.setNationality(country);
                loginModel.setName(username);
                loginModel.setUid(userLoginModel.getUid());
                loginModel.setToken(userLoginModel.getToken());
                loginModel.setSession_cookie(userLoginModel.getSession_cookie());


                if (loginModel != null) {

                    SharedPreferencesHelper.putSharedPreferencesObject(getContext(), SharedPrefConstants.loginObject, loginModel);
                    Utils.successDialog(getContext(),"تم تعديل ملفك الشخصي");
                }else {
                    Toast.makeText(getContext(), "Invalid Error ", Toast.LENGTH_LONG).show();
                    if (Utils.isProgressShowing()){
                        Utils.dismissProccessDialog();
                    }
                }

            }

            @Override
            public void onFailure(String errorResponse) {
                Log.e("sddsfwesd", errorResponse);
                if (Utils.isProgressShowing()){
                    Utils.dismissProccessDialog();
                }
            }
        });
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
