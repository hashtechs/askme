package me.techband.questionapp.contextmenu.interfaces;

import android.view.View;

/**
 * Menu adapter item click listener
 */
public interface OnItemClickListener {

    void onClick(View v);
}
