package me.techband.questionapp.contextmenu;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;


public class TextviewCustomFont extends android.support.v7.widget.AppCompatTextView {
    public TextviewCustomFont(Context context, AttributeSet attrs) {
        super(context, attrs);
            this.setTypeface(Typeface.createFromAsset(context.getAssets(), "DroidArabicKufi.ttf"));
    }
}
