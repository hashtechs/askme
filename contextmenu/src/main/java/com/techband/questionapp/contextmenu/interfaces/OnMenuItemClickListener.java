package me.techband.questionapp.contextmenu.interfaces;

import android.view.View;

/**
 * Menu item click listener
 */
public interface OnMenuItemClickListener {

    void onMenuItemClick(View clickedView, int position);
}
