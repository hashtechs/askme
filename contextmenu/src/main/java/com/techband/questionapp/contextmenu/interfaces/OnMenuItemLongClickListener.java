package me.techband.questionapp.contextmenu.interfaces;

import android.view.View;

/**
 * Menu item long click listener
 */
public interface OnMenuItemLongClickListener {

    void onMenuItemLongClick(View clickedView, int position);
}
